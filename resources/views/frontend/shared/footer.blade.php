<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row footer-top">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="col-sm-6">
                            <img class="img-responsive" src="/frontend/images/logo.png"  alt="Ministerio de Educación">
                        </div>
                        
                    </div>
                    <!-- links redes sociales -->
                    <!--<div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                        <div class="social-wrapper flex-end flex-start-sm">
                            <a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/contacto" class="btn-social img-circle mail js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="contacto" title="Ir a la página de contacto"><i></i></a>
                            <a href="https://twitter.com/EducacionAR" class="btn-social img-circle twitter js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="twitter" title="Ir al perfil en Twitter" target="_blank"><i></i></a>
                            <a href="https://www.facebook.com/educacionAR" class="btn-social img-circle facebook js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="facebook" title="Ir a la página de Facebook" target="_blank"><i></i></a>
                            <a href="https://www.youtube.com/user/educacionar" class="btn-social img-circle youtube js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="youtube" title="Ir al perfil en YouTube" target="_blank"><i></i></a>
                        </div>
                    </div>-->

                    <div class="col-md-6 col-sm-6 col-xs-12 logos-cnt">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="container">
                    <div class="sitemap">
                        <div class="row">
                            <!--<div class="col-sm-6 col-sm-offset-3 sitemap-items flex-column flex-center">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/noticias" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link" data-eventlabel="Noticias"><span>Noticias</span></a>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/seccion/73/que-estamos-haciendo" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link" data-eventlabel="Qué estamos haciendo"><span>Qué estamos haciendo</span></a>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/seccion/256/organizacion" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link" data-eventlabel="Organización"><span>Organización</span></a>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="blue-footer">                 <div class="container">
                    <div class="row flex-align-center">
                        <div class="col-xs-12 col-md-6 ">
                            <address>
                                <p>Ciudad Autónoma de Buenos Aires</p>

                            </address>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <!--<strong class="footer-contact"><a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/contacto" class="js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link" data-eventlabel="contacto">Contacto</a></strong>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-lg-12 footer-bottom flex-center">
                        <p class="space-txt">
                            <strong>www.educacion.gob.ar</strong> es un sitio web oficial del <strong>Gobierno Argentino</strong>
                        </p>
                        <a href="http://www.casarosada.gob.ar/" class="btn-institucional presidencia js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link" data-eventlabel="Presidencia de la nacion"><i></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>