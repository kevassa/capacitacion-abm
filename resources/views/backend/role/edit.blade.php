@extends('backend.layouts.master')

@section('title', 'Editando Rol')

@section('content')

<h3>Editando {{ $role->display_name }}</h3>

{!! Form::model($role, array('route' => array('backend.role.update', $role->id), 'class' => 'form-horizontal', 'id'=>'create_form')) !!} 
    @include('backend/role/shared/form', array('role' => $role))

{!! Form::close() !!}

@endsection
