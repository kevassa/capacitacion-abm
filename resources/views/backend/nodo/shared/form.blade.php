@include('backend.shared.alerts.errors')
<div class="form-group">
    {!! Form::label('nombre', 'Nombre', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::text('nombre', null, array('class' => 'form-control'))  !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('tipo', 'Tipo de Contenido', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-9">
        {!! Form::select('tipo', $tipos, null , array('class' => 'form-control' , 'id' => 'tipo'))  !!}
    </div>
</div>

<div id='inputPagina' hidden>
    <div class="form-group">
        {!! Form::label('cuerpo', 'Cuerpo', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-9">
            {!! Form::text('cuerpo', null, array('class' => 'form-control' , 'id' => 'cuerpo'))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('padre', 'Padre', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-4">
            {!! Form::select('padre', $padres, null , array('class' => 'form-control', 'id' => 'padre'))  !!}
        </div>        
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-5">
        <a href="{{route('backend.nodos')}}" class="btn btn-danger" ><span class="fa fa-arrow-left" aria-hidden="true"></span> Cancelar</a>
        <button type="button" class="btn btn-primary" id='submit_btn'><i class="fa fa-floppy-o"></i> Guardar</button>
    </div>
</div>

<input type="hidden" name="id" value="{{$id}}">

<script type="text/javascript">
    //**************
    //Inicialización
    //**************
    $(document).ready(function(){
        checkTipo();
    });
    
    //Inicialización de CKEditor
    CKEDITOR.config.extraPlugins='html5video,html5audio';
    CKEDITOR.replace('cuerpo');
    CKEDITOR.on('instanceReady', function(ev) {
        $('iframe').contents().click(function(e) {
            if(typeof e.target.href != 'undefined') {
                window.open(e.target.href, 'new' + e.screenX);
            }
        });
    });
    
    //ChosenJS
    
    //*******************
    //Funciones Generales
    //*******************
    
    //TODO: (Posible) displayerrors podria estar en el layout
    function displayErrors  (errors) {
        $("#error-alert").empty();
        $('#error-ul').empty();
        $.each(errors, function () {
            $('#error-ul').append("<li>" + this + "</li>");
        });
        $("#error-alert").html($("#error-template").html());
        $("html, body").animate({scrollTop: 0});   
    }
    
    function validateAjax(){
        $('#cargando').modal('show');
        $.ajax({  
            type: 'POST',
            url : "{{action('Backend\NodosNavegacionController@validateInput')}}",
            async : true,
            data: $('#create_form').serialize(),
            dataType: 'JSON',
            success: function(data) {
                if(data.estado == "OK"){
                    $('#create_form').submit(); 
                }
                else{
                    $('#cargando').modal('hide');
                    var errores = $.map(data.mensajes, function(value, index){
                        return [value];
                    });
                    displayErrors(errores);
                    $('html, body').animate({ scrollTop: 0 }, 'fast');                   
                }
            },
            error: function(data){
                $('#cargando').modal('hide');
                window.alert("FAIL");
            }
        });
    }
    
    //Depende del tipo muestra diferentes campos
    function checkTipo(){
        // 1 => enlace, 2 => pagina
        if ($("#tipo").val() == 1){
            $('#inputPagina').hide();
            $('#padre').prop('disabled',true).trigger("chosen:updated");
            $('#tipo').prop('disabled',true);
        }else{
            $('#inputPagina').show();
            $('#padre').prop('disabled',false).trigger("chosen:updated");
            $('#tipo').prop('disabled',false);
        };
    }
    //*****************
    //Eventos Generales
    //*****************
    
    $('#submit_btn').click(function(){
        validateAjax();
    });
    
    $('#tipo').change(function(){
        checkTipo();
    });
      
</script>
