<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Permission extends EntrustPermission
{
    use SoftDeletes;

    /*
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true; //para que tambien loguee cuando se crea un registro

    public function getCampoRevisionable(){
            return 'display_name'; //los que no se llamen descripcion deberan sobreescribirlo
    }

    public function getDescripcionRevisionable(){
            $campoRevisionable = $this->getCampoRevisionable();
            return $this->$campoRevisionable;
    }
     * 
     */
    
    public function scopeLikeUpper($query, $column, $value)
    {
        if(!is_null($value)){
            return $query->where(DB::raw('upper('.$column.')'),'LIKE', '%'.strtoupper($value).'%');
        }else{
            return $query;
        }
    }
    
    
    
    
}