@extends('backend.layouts.master') 

@section('content')
<br><br>
    <div style="text-align: center;">
	    <div class="alert alert-danger" style="font-size:20px; margin-left:70px"> 
		<strong>¡Error!</strong> No se puede acceder a la página solicitada
            </div>
        <center>
            <div class="container">
                <img src="{{asset('backend/images/stop.jpg')}}" alt="ERROR" class="image img-responsive">
            </div>
            <a href="{{ route('backend.home') }}" class="btn btn-basic fa fa-backward" title="Ir al Inicio" style="font-size:30px; margin-left:50px"> Ir al Inicio</a>
        </center>
</div>
@endsection