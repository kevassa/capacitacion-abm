<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;

use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{

    public function index(Request $request)
    {
    	
    	$data = $request->input();
    	
    	$name = isset($data['name']) ? $data['name'] : null;
    	$display_name = isset($data['display_name']) ? $data['display_name'] : null;
    	$description = isset($data['description']) ? $data['description'] : null;
    	$perm_id = isset($data['perm_id']) && trim($data['perm_id']) != "0" ? $data['perm_id'] : null;
        
        $sort= isset($data['s']) ? $data['s'] : 'id';
        $dir= isset($data['o']) ? $data['o'] : 'asc';
        
    	$roles = Role::orderBy($sort, $dir)
                	->likeUpper('name', $name)
                	->likeUpper('display_name', $display_name)
                	->likeUpper('description', $description)
                	->hasPermission($perm_id)
                	;
    	
    	\Session::put('rolesList', $roles->get());
        
        $permissions = $this->add_default_option(Permission::orderBy('display_name')->lists('display_name', 'id'));
        
        return view('backend/role/list')
                ->with('roles', $roles->paginate())
                ->with('permissions', $permissions)
                ->with('name', $name)
                ->with('display_name', $display_name)
                ->with('description', $description)
                ->with('perm_id', $perm_id)
        ;
    }
    
    public function xls(){
    	return \Excel::create('Roles', function($excel) {
    		 
    		$excel->sheet('Hoja 1', function($sheet) {
    			$sheet->row(1, array('ID', 'NOMBRE', 'DISPLAY NAME', 'DESCRIPCION', 'PERMISOS'));
    			$index = 2;
    			foreach (\Session::get('rolesList') as $tp){
    				$sheet->row($index, array(
    						$tp->id,
    						$tp->name,
    						$tp->display_name,
    						$tp->description,
    						$tp->permissionsDesc
    				));
    				$index++;
    			}
    		});
    
    	})->download('xls');
    }

    public function create()
    {
    	$permissions = Permission::orderBy('display_name')->get()->lists('display_name', 'id');
        return view ('backend/role/create')
	        ->with('permissions', $permissions)
	        ->with('permissionsSelected', array())
        ;
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::orderBy('display_name')->get()->lists('display_name', 'id');
        
        return view ('backend/role/edit')
        	->with('role', $role)
        	->with('permissions', $permissions)
        	->with('permissionsSelected', array_column($role->perms->toArray(), 'id'))
        ;
    }

    public function store(Request $request)
    {
        $data = $request->input();
        $v = Validator::make($data, Role::rules($data['id']));
        if ($v->fails()){
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $r = Role::create($data);
        
        if(isset($data['perms']))
        $r->attachPermissions($data['perms']);
        
        return redirect()->route('backend.role')
                ->with('message', trans('abms_core.created', ['name' => trans('abms_core.role')]));
    }
    
    public function update(Request $request, $id)
    {
        $data = $request->input();
        
        $role = Role::findOrFail($id);
        $v = Validator::make($data, Role::rules($data['id']));
        if ($v->fails()){
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $role->update($data);
        
        if(isset($data['perms']))
        $role->savePermissions($data['perms']);
        
        return redirect()->route('backend.role')
                ->with('message', trans('abms_core.updated', ['name' => trans('abms_core.role')]));
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        
        if($role->users->count() > 0){
            return redirect()
                ->back()
                ->with('error', trans('abms_core.dependency_constraint', ['name' => trans('abms_core.role'), 'dependency' => trans('abms_core.user')]));
        }
        
        $role->delete();
        
        return redirect()->route('backend.role')
                ->with('message', trans('abms_core.deleted', ['name' => trans('abms_core.role')]));
    }
    
    public function validateInput(Request $request){
        $data = $request->input();
        $v = Validator::make($data, Role::rules($data['id']));
        if ($v->fails()){
           return response()->json(['estado' => "error", 'mensajes' => $v->errors()]);
        }
        return response()->json(['estado' => "OK", 'mensajes' => []]);
    }
    
}
