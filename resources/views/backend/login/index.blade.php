@extends('backend.layouts.empty')

@section('title', 'Ingreso al Sistema')

@section('content')


@include('backend/shared/alerts/messages')

<div class="container">    
        <div id="loginbox" style="margin-top:20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                
                    <div class="panel-heading">
                        <div class="panel-title" style="text-align:center;"><b>Ingreso a la Plataforma de Administración</b></div>
                        <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Olvid&oacute; la password?</a></div> -->
                    </div> 
                <center> 
                        <!--<img src="/frontend/images/logo.jpg" alt="Logo" class="image img-responsive" style='height:145px; padding-top:15px'>--> 
                    <span style="font-size:44px"><b>#TomáCarrera</b></span>
                </center>
                
                @if (!$errors->isEmpty())
                <br>
                    <div class="alert alert-danger alert-dismissible" role="alert" style="text-align:center">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span> El usuario y/o la contraseña son incorrectos.</span>
                        <span> Por favor intente nuevamente. </span>
                    </div>
                @endif
                    <div style="padding-top:15px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        {!! Form::open(array('route' => 'backend.login.checkLogin', 'method' => 'POST', 'class' => 'form-horizontal')) !!}
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user faa-passing" style="width:15px"></i></span>                                    
                                        {!! Form::text('username', $username, array('class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Usuario (e-mail)'))  !!}
                            </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key faa-passing" style="width:15px"></i></span>
                                {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Contraseña'))  !!}
                            </div>


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <input type="submit" id="btnSubmit" value="Ingresar" class="btn btn-primary btn-lg btn-block btn-signin" />
                                    </div>
                                    <br><br><br>
                                    <span style="font-size:16px;float:right">
                                        <a href="{{route('backend.login.recovery')}}">¿Olvidaste tu contraseña?</a>
                                    </span>
                                </div>
                            {!! Form::close() !!}    
                        </div>  
                    </div>  
        </div>
        
    </div>

@endsection