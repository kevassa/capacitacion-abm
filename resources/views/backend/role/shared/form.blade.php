@include('backend.shared.alerts.errors')
<?php $id = isset($role) ? $role->id : "NULL";?>
{!! Form::hidden('id', $id)  !!}
<div class="form-group">
    {!! Form::label('name', 'Nombre', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::text('name', null, array('class' => 'form-control'))  !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('display_name', 'Display Name', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::text('display_name', null, array('class' => 'form-control'))  !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('description', 'Descripcion', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::text('description', null, array('class' => 'form-control'))  !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('perms', 'Permisos', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::select('perms', $permissions, $permissionsSelected, ['multiple'=>'multiple','name'=>'perms[]','class' => 'form-control' ,'id'=>"permissions"]) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-5">
        <a href="{{route('backend.role')}}" class="btn btn-danger" ><span class="fa fa-arrow-left" aria-hidden="true"></span> Cancelar</a>
        <button type="button" class="btn btn-primary" id='submit_btn'><i class="fa fa-floppy-o"></i> Guardar</button>
    </div>
</div>

<div class='modal fade' id="cargando"  data-keyboard="false" data-backdrop="static" >
    <div class="modal-dialog modal-lg" role="document" style="padding-top:10%">
        <div class="modal-content" >
            <div class="modal-body" style="text-align:center">
                <span> Cargando...</span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#permissions").chosen();
    var displayErrors = function (errors) {
        $("#error-alert").empty();
        $('#error-ul').empty();
        $.each(errors, function () {
            $('#error-ul').append("<li>" + this + "</li>");
        });
        $("#error-alert").html($("#error-template").html());
        $("html, body").animate({scrollTop: 0});   
}
$('#submit_btn').click(function(){
        $('#cargando').modal('show');
        $.ajax({  
            type: 'POST',
            url : "{{action('Backend\RolesController@validateInput')}}",
            async : true,
            data: $('#create_form').serialize(),
            dataType: 'JSON',
            success: function(data) {
                if(data.estado == "OK"){
                    $('#create_form').submit(); 
                }
                else{
                    $('#cargando').modal('hide');
                    var errores = $.map(data.mensajes, function(value, index){
                        return [value];
                    });
                    displayErrors(errores);
                    $('html, body').animate({ scrollTop: 0 }, 'fast');                   
                }
            },
            error: function(data){
                $('#cargando').modal('hide');
                window.alert("FAIL");
            }
        });
    });
</script>
