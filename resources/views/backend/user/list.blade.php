@extends('backend.layouts.master')

@section('title', 'Gestión de Accesos')

@section('content')

<h1>Configuración</h1>
<h3>Gestión de Accesos</h3>

@include('backend/shared/alerts/messages')

<div class="well">
        <form class="form-inline">   
            <div class="row">
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('username', 'Username', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('username', $username, array('class' => 'form-control col-sm-6', 'id' => 'username', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('name', 'Nombre', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('name', $name, array('class' => 'form-control col-sm-6', 'id' => 'name', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('lastname', 'Apellido', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('lastname', $lastname, array('class' => 'form-control col-sm-6', 'id' => 'lastname', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('role_id', 'Rol', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::select('role_id', $roles, $role_id, ['class' => 'form-control col-sm-6', 'style' => 'width:200px;']) !!}
                     </div>        
                   </div>
                </div>
            </div>
            <br/>
            
            <div style="text-align:right;">
                <br/>
                <button type="submit" class="btn btn-info">Buscar</button>        
                <button type="button" onclick="$('#username').val('');$('#name').val('');$('#lastname').val('');$('#role_id').val('0');" class="btn btn-primary">Limpiar</button>        
            </div>       
        </form>
</div>

<div class="pull-right">
	<a href="{{route('backend.user.xls')}}" class="btn btn-primary fa fa-print" title="Exportar datos a XLS"> XLS</a>
    @permission('CREAR_USUARIO')
        <a href="{{route('backend.user.add', ['pesoProvincia', null])}}" class="btn btn-primary fa fa-plus"> Agregar</a>
    @endpermission
</div>
<br/><br/>

<table class="table table-bordered table-striped table-condensed" style="table-layout: fixed;">
    <thead>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('id', 'ID') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('username', 'Username') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('name', 'Nombre') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('lastname', 'Apellido') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('role', 'Rol') !!}</th>
        <th>Acciones</th>
    </thead>
    <tbody>
        
        @foreach($users as $u)
            <tr style="word-wrap: break-word">
                <td>{{ $u->id }}</td>
                <td>{{ $u->username }}</td>
                <td>{{ $u->name }}</td>
                <td>{{ $u->lastname }}</td>
                <td>{{ $u->rolesDesc }}</td>
                <td>@include('backend/user/shared/actions', array('user' => $u))</td>
            </tr>

        @endforeach
        @if(count($users)==0)
            <tr>
                <td colspan="6">
                    <center>No se han encontrado resultados para la búsqueda.</center>
                </td>
            </tr>
        @endif
    </tbody>
</table>

<!-- Solicitar siempre confirmación -->
<div class="modal fade" id="confirmarEliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Confirmaci&oacute;n</h4>
        </div>
        <div class="modal-body">
            <h3 id='lblEliminar'></h3>
            Una vez eliminado sus datos no se visualizaran en el sistema
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" id="butConfirmarEliminar">Aceptar</button>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    
    var eliminarForm;
    
    $('.frmEliminar').unbind().click(function(){
        
        var descripcion = $(this).attr('data-user-username');
        
        eliminarForm = $(this).parent();
        
        $('#lblEliminar').html('¿Confirma que desea eliminar ' + descripcion + '?');
        $('#confirmarEliminar').modal('show');
        
    });
    
    $('#butConfirmarEliminar').click(function(){
        eliminarForm.submit();
    });
    
</script>

{!! $users->appends(Input::except('page'))->render() !!}
@endsection