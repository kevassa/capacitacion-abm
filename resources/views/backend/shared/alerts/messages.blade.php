@if (Session::get('message'))

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Informaci&oacute;n:</strong> {{ Session::get('message') }}
    </div>

@endif