<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;

use DB;

class ModeloBase extends Model {
    use SoftDeletes;
    
    private $apiUrlDailyMotion = "http://www.dailymotion.com/services/oembed?url=%s";
    private $apiUrlVimeo = "https://vimeo.com/api/oembed.json?url=%s";
    private $apiUrlYoutube = "http://www.youtube.com/oembed?url=%s";
    
    protected $arryableKey = ',';
    
    private $elementos = ["á"=>"a","é"=>"e","í"=>"i","ó"=>"o","ú"=>"u", "Á"=>"A", "É"=>"E", "Í"=>"I", "Ó"=>"O", "Ú"=>"U"];
        
    public function setAttribute($key, $value) {
    	if(isset($this->dates) && in_array($key, $this->dates) && $value != null) {
    		$value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    	}
    	
    	if(isset($this->nullables) && in_array($key, $this->nullables) && !is_array($value)) {
    		$value = trim($value) == '' ? null : $value;
    	}
    	
    	if(isset($this->arrayables) && in_array($key, $this->arrayables)) {
    		$value = implode($this->arryableKey, $value);
    	}
    	
    	return parent::setAttribute($key, $value);
    }
    
    public function getAttributeValue($key) {
    	$value = parent::getAttributeValue($key);
    	    	
    	if(isset($this->dates) && in_array($key, $this->dates)) {
    		return empty($value) ? null : (new Carbon($value))->format('d/m/Y');
    	}
    	
    	if(isset($this->arrayables) && in_array($key, $this->arrayables)) {
    		return $value != null ? explode($this->arryableKey, $value) : null;
    	}
    	
    	return $value;
    }      
    
    public function sync($relacion, $data) {
    	$sync = array();
    	 
    	$actuales = $relacion->lists('id')->toArray();    	
    	 
    	if(isset($data)) {
    		foreach ($actuales as $item) {
    			if (!in_array($item, $data)) {
    				$sync[$item] = ['deleted_at' => DB::raw('NOW()')];
    			}
    		}
    		
    		foreach ($data as $item) {
    			if (!in_array($item, $actuales)) {
    				$sync[$item] = ['created_at' => DB::raw('NOW()'), 'deleted_at' => null];
    			}
    			else {
    				$sync[$item] = ['deleted_at' => null];
    			}
    		}    		     	
    	}
    	
    	if (!empty($sync)) {
    	 	$relacion->sync($sync);
    	} 
    	else {
    		$relacion->detach();
    	}    	       	 
    }
    
    public function scopeLikeUpper($query, $column, $value)
    {
        if(!is_null($value)){
            $value = strtr ($value, $this->elementos);                       
            return $query->where(DB::raw("TRANSLATE(upper(".$column."), 'áéíóúÁÉÍÓÚçÇ','aeiouAEIOUcC')"),'LIKE', '%'.strtoupper($value).'%');
        }else{
            return $query;
        }
    }
    
         public function urlVideo(){
        
        $url = $this->video;
        $urlEmbed = "";
        
        if (false !== ($urlEmbed = $this->dailyMotionUrl($url)))
            return $urlEmbed;
        
        if (false !== ($urlEmbed = $this->vimeoUrl($url)))
            return $urlEmbed;
        
        if (false !== ($urlEmbed = $this->youtubeUrl($url)))
            return $urlEmbed;
        
        return $url;
    }
    private function dailyMotionUrl($url) {
        if (preg_match('!^.+dailymotion\.com/(video|hub)/([^_]+)[^#]*(#video=([^_&]+))?|(dai\.ly/([^_]+))!', $url, $m)) {
            return $this->callWS($this->apiUrlDailyMotion, $url);
        }
        return false;
    }
    
    private function getUrl($html){
        $s = substr($html, strpos($html, 'src="') + 5);
        $url = substr($s, 0, strpos($s, '"'));
        return $url;
    }

    private function vimeoUrl($url) {
        if (preg_match('#(?:https?://)?(?:www.)?(?:player.)?vimeo.com/(?:[a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $m)) {
            return $this->callWS($this->apiUrlVimeo, $url);
        }
        return false;
    }
    
    private function callWS($baseUrl, $urlVideo){
        $client = new Client();

        $response = null;
        
        try {
            $response = $client->get(sprintf($baseUrl, $urlVideo));
            
            $r = json_decode($response->getBody());
            
            if ($response->getStatusCode() != 200)
                return false;
        
            if (isset($r->type) && $r->type == "video")
                return $this->getUrl($r->html);
            
            return false;
            
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return false;
        }
            
    }
    
     private function youtubeUrl($url) {
        $parts = parse_url($url);
        if (isset($parts['host'])) {
            $host = $parts['host'];
            if (false === strpos($host, 'youtube') && false === strpos($host, 'youtu.be')) {
                return false;
            }
        }
        
        return $this->callWS($this->apiUrlYoutube, $url);
    }
    
    public static function attributeNum($key){
        switch($key){
            case(0):
                $string="primer";
                break;
            case(1):
                $string="segundo";
                break;
            case(2):
                $string="tercer";
                break;
            case(3):
                $string="cuarto";
                break;
            case(4):
                $string="quinto";
                break;
        }
        return $string;
    }
}