<?php

namespace App\Http\Controllers\Frontend; 

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Editorial;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

use App;
use PDF;
use Storage;
class InscripcionController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend/inscripcion/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('max_execution_time', 180);
        $data = array_map('strtolower', $request->input());
        $u=User::where("username","=",$data["email"])->first();
        $v = Validator::make($data, Editorial::$rules);
        if ($v->fails()) {
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        if($u){
           return redirect()->back()->withInput()->withErrors( ['El correo electrónico ya ha sido registrado.']);
        }
        $e=Editorial::create($data);
        
        $user['name'] = $data['oferente'];
        $user['lastname'] = $data['oferente'];
        $user['username'] = $data['email'];
        $user['editorial_id']= $e->id;
        $password=$this->generateRandomString();
        $user['password'] = \Hash::make($password);
        $u = User::create($user);
        $roleEditorial = Role::where('name', '=', Role::EDITORIAL)->first();
        $u->attachRole($roleEditorial);
        
        $view =  \View::make('frontend/inscripcion/shared/pdf', ["e" => $e])->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $path=storage_path().DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "formulario_de_inscripcion.pdf";
        $pdf->save($path);
        \Mail::send('frontend/inscripcion/shared/inscripcionOkay',
        [   'pass' => $password,
            'name'=> $u->name,
            'email'=>$e->email,
            'pathLogoLPA' => public_path().DIRECTORY_SEPARATOR."frontend".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."logo-LPA.jpg",
            'pathLogoMinisterio' => public_path().DIRECTORY_SEPARATOR."frontend".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."logo.png"
        ],
        function ($m) use($e, $path){
                $m->from(config('mail.from')['address'], config('mail.from')['name']);
                $m->to($e->email)->subject('¡Se inscribió con éxito!');
                $m->attach($path);
        });
        \File::delete($path);
        return redirect()->route('frontend.inscripcion')
                ->with('message', '¡Se inscribió correctamente! Recibirá un correo electrónico con información detallada a la brevedad.');
    }
    
    public function validateInscripcion(Request $request){
        $data = array_map('strtolower', $request->input());
        $u=User::where("username","=",$data["email"])->first();
        $v = Validator::make($data, Editorial::$rules, Editorial::messages());
        if ($v->fails()) {
           return response()->json(['estado' => "error", 'mensajes' => $v->errors()]);
        }
        if($u){
           return response()->json(['estado' => "error", 'mensajes' => ['El correo electrónico ya ha sido registrado, para recuperar su contraseña <a href="'.route('backend.login.recovery').'">haga click aqui</a>']]);
        }
        return response()->json(['estado' => "OK", 'mensajes' => []]);
    }
}
