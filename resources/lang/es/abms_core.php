<?php

return [
    'created' => 'El :name fue creado exitosamente.',
    'updated' => 'El :name fue modificado exitosamente.',
    'deleted' => 'El :name fue eliminado exitosamente.',
    
    'dependency_constraint' => 'El :name no puede eliminarse ya que se encuentra asociado a un :dependency activo. Por favor verifíquelo.',

	'qualification_save' => 'Las notas del :name :title fueron guardadas correctamente.',
	'course_evaluado' => 'El :name :title fue evaluado correctamente.',
		
	'assigned_format' => 'El :name :title ya posee un formato asignado.',
		
	'not_assigned_format' => 'El :name :title no posee un formato asignado.',
			
    'certification_type' => 'tipo de certificación',	
    'course' => 'curso',	
    'duration_type' => 'tipo de duración',	
    'dynamic_type' => 'tipo de dinámica',	
    'education_level' => 'nivel de educación',	
    'field' => 'campo',	
    'geographical_scope' => 'alcance geográfico',	
    'modality' => 'modalidad',	
    'model_course' => 'curso-modelo',	
    'permission' => 'permiso',	
    'receiver' => 'destinatario',	
    'role' => 'rol',	
    'thematic_content' => 'contenido temático',	
    'user' => 'usuario',	
    'inscription' => 'inscripción',
    'model_course_type' => 'tipo de oferta formativa',
    'education_modality' => 'modalidad del sistema educativo',
    'resource' => 'recurso',
    'challenge' => 'desafio',
    'reincidencia_cursados' => 'Reincidencia en Cursos ya Cursados',
    'cantidad_simultaneo' => 'Cantidad de cursos simultaneos por inscripto',
    'repetir_aprobado' => '¿El usuario puede repetir cursos aprobados?'
];