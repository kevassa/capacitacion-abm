@ability('', 'VISUALIZAR_USUARIOS, CREAR_USUARIO, VISUALIZAR_PERMISOS, VISUALIZAR_ROLES, CREAR_ROL')
    <div class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Configuración <span class="caret"></span></a>
        <ul class="dropdown-menu">
            @permission('VISUALIZAR_USUARIOS')
                <li>
                    <a href="{{route('backend.user')}}">
                        Accesos
                    </a>
                </li>
            @endpermission
            @permission('VISUALIZAR_PERMISOS')
                <li>
                    <a href="{{route('backend.permission')}}">
                        Permisos
                    </a>
                </li>
            @endpermission
            @permission('VISUALIZAR_ROLES')
                <li>
                    <a href="{{route('backend.role')}}">
                         Roles
                    </a>
                </li>
            @endpermission
        </ul>
    </div>
@endability