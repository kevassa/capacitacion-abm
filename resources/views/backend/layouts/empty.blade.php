<!DOCTYPE html>
<html lang="es" id="login-page">
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta name="description" content="">
<!--         <meta name="author" content="Educar"> -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ elixir('backend/css/all.css') }}">
        <script type="text/javascript" src="{{ elixir('backend/js/all.js') }}"></script>
        
    </head>
    <body>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="margin-top:50px;">
                @yield('content')
            </div>
            <div class="col-md-1"></div>
        </div>
    </body>
</html>