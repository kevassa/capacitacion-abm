<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

/*
*  Tests unitarios para la fuerza de contraseña de un usuario.
*  La contraseña debe ser de 6 caracteres, contener mayusculas, minusculas y un numero o un simbolo.
*/
class UserPassStrengthTest extends TestCase
{
    //*******************
    //Parámetros globales
    //*******************
    //<editor-fold> 
    private $data = array(
                    'id' => 1 ,
                    'username' => 'admin',
                    'name'     => 'Administrador',
                    'lastname' => 'Administrador',
                    'roles' => [1]);
    //</editor-fold> 
    
    //**************
    //Tests de fallo
    //**************
    //<editor-fold> 
    public function testPasswordFailSinSeisLetras()
    {
        $this->data["password"] = "Adm#1";
        $v = Validator::make($this->data, User::rules($this->data['id']));
        $this->assertTrue($v->fails());
    }
    
    public function testPasswordFailSinMayuscula()
    {
        $this->data["password"] = "admin#1";
        $v = Validator::make($this->data, User::rules($this->data['id']));
        $this->assertTrue($v->fails());
    }
    
    public function testPasswordFailSinMinuscula()
    {
        $this->data["password"] = "ADMIN#1";
        $v = Validator::make($this->data, User::rules($this->data['id']));
        $this->assertTrue($v->fails());
    }
    
    
    public function testPasswordFailSinNumeroOSimbolo()
    {
        $this->data["password"] = "Adminn";
        $v = Validator::make($this->data, User::rules($this->data['id']));
        $this->assertTrue($v->fails());
    }
    
    //</editor-fold> 
    
    //**************
    //Tests de exito
    //**************
    //<editor-fold> 
    public function testPasswordSuccessNumero()
    {
        $this->data["password"] = "Admin1";
        $v = Validator::make($this->data, User::rules($this->data['id']));
        $this->assertTrue(!$v->fails());
    }
    
    public function testPasswordSuccessSimbolo()
    {
        $this->data["password"] = "Admin#";
        $v = Validator::make($this->data, User::rules($this->data['id']));
        $this->assertTrue(!$v->fails());
    }
    //</editor-fold> 
}
