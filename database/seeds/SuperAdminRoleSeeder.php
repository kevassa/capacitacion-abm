<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class SuperAdminRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRole(Role::SUPERADMIN, 'Super Administrador', 'Superadministrador del sistema.', Permission::all());
        
    }
    
    private function createRole($name, $display_name, $description, $permissions){
        $r = Role::where('name', '=', $name)->first();
        if(!$r){
            $r = new Role();
        }
        $r->name = $name;
        $r->display_name = $display_name;
        $r->description = $description;
        
        $r->save();
        $r->perms()->detach();
        
        foreach($permissions as $p){
        	$r->attachPermission($p);
        }
    }
}
