@extends('backend.layouts.master')

@section('title', 'Agregando Usuario')

@section('content')

<h3>Agregando un Usuario</h3>


{!! Form::open(array('route' => 'backend.user.save', 'class' => 'form-horizontal', 'id'=>'create_form')) !!}
    @include('backend/user/shared/form')
{!! Form::close() !!}

@endsection
