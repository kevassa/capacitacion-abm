@include('frontend/shared/header')
    <body>
            @section('header-provider')
                @include('frontend/shared/header-provider')
            @show
            <div class="container">@yield('breadcrumb')</div>
            <section>
                @yield('central-content')
            </section>
            @include('frontend/shared/footer')
    </body>
</html>