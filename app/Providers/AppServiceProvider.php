<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('url_valid', function($attribute, $value) {
            return preg_match('/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/', $value);
        });
        
        Validator::extend('alpha_spaces', function($attribute, $value) {
            return preg_match('/(^[A-Za-z ]+$)+/', $value);
        });
        Validator::extend('numbers_only', function($attribute, $value) {
            return preg_match('/^[0-9]+$/', $value);
        });
        
        //Verifica si la contraseña es de 6 caracteres, contiene mayusculas, minusculas y un numero o un simbolo
        Validator::extend('strong_password', function($attribute, $value) {
            if(strlen($value)<6) return false;
            if(!preg_match('/[a-z]/', $value)) return false;
            if(!preg_match('/[A-Z]/', $value)) return false;
            if(!preg_match('/[0-9]/',$value) && !preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/',$value))return false;
            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
