{!! Form::model($user, ['route' => ['backend.user.delete', $user->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@permission('ELIMINAR_USUARIO')
<a href="#" class="btn btn-danger frmEliminar" data-toggle="tooltip" data-placement="bottom" data-user-username='{{ $user->username }}' data-user-id='{{ $user->id }}' title="Eliminar {{ $user->username }}">
    <i class="fa fa-eraser"></i>
</a>
@endpermission
@permission('EDITAR_USUARIO')
<a href="{{route('backend.user.edit', ['id' => $user->id])}}" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Editar {{ $user->username }}">
    <i class="fa fa-edit"></i>
</a>
@endpermission
{!! Form::close() !!}