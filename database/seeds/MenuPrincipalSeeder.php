<?php

use Illuminate\Database\Seeder;
use App\Models\NodoNavegacion;

class MenuPrincipalSeeder extends Seeder
{
    /**
     * Seeder de nodo menu principal.
     *
     * @return void
     */
    public function run()
    {
        $nodo = new NodoNavegacion();
        $nodo->nombre= "Menú Principal";
        $nodo->save();
    }
    
    
}
