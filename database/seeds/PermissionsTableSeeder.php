<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Seguridad
        $this->addPermission('VISUALIZAR_USUARIOS', 'Visualizar Usuarios');
        $this->addPermission('CREAR_USUARIO', 'Crear Usuario');
        $this->addPermission('EDITAR_USUARIO', 'Editar Usuario');
        $this->addPermission('ELIMINAR_USUARIO', 'Eliminar Usuario');
        
        $this->addPermission('VISUALIZAR_ROLES', 'Visualizar Roles');
        $this->addPermission('CREAR_ROL', 'Crear Rol');
        $this->addPermission('EDITAR_ROL', 'Editar Rol');
        $this->addPermission('ELIMINAR_ROL', 'Eliminar Rol');
        
        $this->addPermission('VISUALIZAR_PERMISOS', 'Visualizar Permisos');
        
    }
        
    private function addPermission($name, $description)
    {
        $p = Permission::where('name', '=', $name)->first();
        if(!$p){
            $p = new Permission();
        }
        $p->name = $name;
        $p->display_name = $name;
        $p->description = $description;
        $p->save();
    }
}
