<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRole(Role::ADMINISTRADOR, 'Administrador', 'Administrador general del sistema.', $this->getPermisosAdministrador());
        $this->createRole(Role::SUPERADMIN, 'Super Administrador', 'Superadministrador del sistema.', Permission::all());
        
    }
    
    private function createRole($name, $display_name, $description, $permissions){
        $r = Role::where('name', '=', $name)->first();
        if(!$r){
            $r = new Role();
        }
        $r->name = $name;
        $r->display_name = $display_name;
        $r->description = $description;
        
        $r->save();
        $r->perms()->detach();
        
        foreach($permissions as $p){
        	$r->attachPermission($p);
        }
    }
    
    public static function getPermisosAdministrador(){
        
        $permisos = [
            'VISUALIZAR_USUARIOS', 'CREAR_USUARIO', 
            'EDITAR_USUARIO', 'ELIMINAR_USUARIO', 
            'VISUALIZAR_ROLES','CREAR_ROL', 
            'EDITAR_ROL', 'ELIMINAR_ROL', 
            'VISUALIZAR_PERMISOS'
        ];
        
        return Permission::whereIn('name', $permisos)->get();
        
    }
    
}
