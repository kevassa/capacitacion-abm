@if (Session::get('message'))
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Informaci&oacute;n:</strong> {{ Session::get('message') }}
    </div>
@endif	

@if (!$errors->isEmpty())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Atenci&oacute;n:</strong> Corrija los errores que figuran debajo para continuar
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="alert alert-danger" role="alert" style="display: none">
    <button type="button" class="close" id="erroresClose" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Atenci&oacute;n:</strong> Corrija los errores que figuran debajo para continuar:
</div>

<div id="error-alert"></div>
<div id="error-template" style="display:none">
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>
        <strong>Atenci&oacute;n:</strong> Corrija los errores que figuran debajo para continuar:
        <ul id="error-ul"></ul>
    </div>
</div>