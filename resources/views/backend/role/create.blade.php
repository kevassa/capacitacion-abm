@extends('backend.layouts.master')

@section('title', 'Agregando Rol')

@section('content')

<h3>Agregando un Rol</h3>

{!! Form::open(array('route' => 'backend.role.save', 'class' => 'form-horizontal', 'id'=>'create_form')) !!}
    @include('backend/role/shared/form')

{!! Form::close() !!}

@endsection
