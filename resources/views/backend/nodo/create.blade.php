@extends('backend.layouts.master')

@section('title', 'Nuevo Contenido')

@section('content')
<h1>Contenidos</h1>
<h3>Nuevo Contenido</h3>

{!! Form::open(array('route' => 'backend.nodos.save', 'class' => 'form-horizontal', 'id'=>'create_form')) !!}
    @include('backend/nodo/shared/form')

{!! Form::close() !!}

@endsection
