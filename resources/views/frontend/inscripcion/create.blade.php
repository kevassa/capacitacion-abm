@extends('frontend.layouts.home') 

@section('title', 'Sistema de Gesti&oacute;n de Inscripciones') 
@section('section-name', 'Inicio') 

@section('tab-content')
<style>
        .dropdown-menu {
            background-color: #fff !important;
            border: 1px solid #ccc !important;
            border: 1px solid rgba(0,0,0,.15) !important;
            border-radius: 4px !important;
            box-shadow: 0 6px 12px rgba(0,0,0,.175) !important;
        }
        
        .disclaimer{
            margin-left: 50px;
        }
    </style>
    <div class="container">
	@if (Session::get('message'))
	    <div class="alert alert-info alert-dismissible" role="alert">
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <strong>Informaci&oacute;n:</strong> {{ Session::get('message') }}
	    </div>
	@endif	
        <div class="row">
            <h1>Inscripción</h1>
            {!! Form::open(array('route' => 'frontend.inscripcion.add', 'class' => 'form-horizontal', 'id'=>'create_form')) !!}
                
            {!! Form::close() !!}
        </div>        
    </div>  
@endsection
