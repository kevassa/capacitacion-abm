@include('backend.shared.alerts.errors')
<?php 
    $id = isset($user) ? $user->id : "NULL";
?>
{!! Form::hidden('id', $id)  !!}
@if(!isset($micuenta))
<div class="form-group">
    {!! Form::label('username', 'Username', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::text('username', null, array('class' => 'form-control'))  !!}
    </div>
</div>
@else
        {!! Form::hidden('username', null, array('class' => 'form-control'))  !!}
        {!! Form::hidden('micuenta', null, array('class' => 'form-control'))  !!}
@endif
<div class="form-group">
    {!! Form::label('name', 'Nombre', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::text('name', null, array('class' => 'form-control'))  !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('lastname', 'Apellido', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::text('lastname', null, array('class' => 'form-control'))  !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('password', 'Password', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::password('password', array('class' => 'form-control', 'id' => 'password'))  !!}
    </div>
    <a href="#" class="btn btn-primary" id="changePassword" style="display:none;"><span class="fa fa-exchange" aria-hidden="true"></span> Cambiar</a>
</div>


@if(!isset($micuenta))

<div class="form-group">
    {!! Form::label('roles', 'Roles', array('class' => 'col-sm-2 control-label')) !!}
    <div class="col-sm-4">
        {!! Form::select('roles', $roles, $rolesSelected, ['multiple'=>'multiple','name'=>'roles[]','class' => 'form-control']) !!}
    </div>
</div>
@else
 {!! Form::hidden('roles', null, array('class' => 'form-control'))  !!}
@endif

    
{!! Form::hidden('change_password', null, array('id' => 'change_password', 'readonly' => true, 'class' => 'form-control typeahead'))  !!}


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-5">
        @if(!isset($micuenta))
        <a href="{{route('backend.user')}}" class="btn btn-danger" ><span class="fa fa-arrow-left" aria-hidden="true"></span> Cancelar</a>
        @else
        <a href="{{route('backend.home')}}" class="btn btn-danger" ><span class="fa fa-arrow-left" aria-hidden="true"></span> Cancelar</a>
        @endif
        <button type="button" class="btn btn-primary" id='submit_btn'><i class="fa fa-floppy-o"></i> Guardar</button>
    </div>
</div>
<div class='modal fade' id="cargando"  data-keyboard="false" data-backdrop="static" >
    <div class="modal-dialog modal-lg" role="document" style="padding-top:10%">
        <div class="modal-content" >
            <div class="modal-body" style="text-align:center">
                <span> Cargando...</span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
	if($("#editando").length){
		$("#password").prop( "disabled", true );
		$("#changePassword").show();
	
		$("#changePassword").click(function(e){
			e.preventDefault;
			$("#change_password").val("1");
			$("#password").val("");
			$("#password").prop( "disabled", false );
		});
	}
});

 var displayErrors = function (errors) {
        $("#error-alert").empty();
        $('#error-ul').empty();
        $.each(errors, function () {
            $('#error-ul').append("<li>" + this + "</li>");
        });
        $("#error-alert").html($("#error-template").html());
        $("html, body").animate({scrollTop: 0});   
}
$('#submit_btn').click(function(){
        $('#cargando').modal('show');
        $.ajax({  
            type: 'POST',
            url : "{{action('Backend\UsersController@validateInput')}}",
            async : true,
            data: $('#create_form').serialize(),
            dataType: 'JSON',
            success: function(data) {
                if(data.estado == "OK"){
                    $('#create_form').submit(); 
                }
                else{
                    $('#cargando').modal('hide');
                    var errores = $.map(data.mensajes, function(value, index){
                        return [value];
                    });
                    displayErrors(errores);
                    $('html, body').animate({ scrollTop: 0 }, 'fast');                   
                }
            },
            error: function(data){
                $('#cargando').modal('hide');
                window.alert("FAIL");
            }
        });
    });
</script>
