<?php

namespace App\Models;

use App\Models\ModeloBase;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Config;

use Illuminate\Database\Eloquent\Model;

use Zizaco\Entrust\Traits\EntrustUserTrait;

use App\Models\Permission;
use App\Models\Role;

use Carbon\Carbon;

use DB;

/**
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
//     use SoftDeletes;
    use Authenticatable, CanResetPassword, EntrustUserTrait;
    
    protected $table = 'users';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'name', 
        'lastname', 
        'password',
        'provincia_id',
        'editorial_id'
    ];
    
    public static function rules($id){ 
        return [
            'username'      =>          'required|string|max:255|unique:users,username,' . $id,
            'name'          =>          'required|string|alpha_spaces|max:255',
            'password'      =>          'required|strong_password',
            'lastname'      =>          'required|string|max:255',
            'roles'         =>          'required'
        ];
    }
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    public function getRolesDescAttribute(){
        $roles = array();
        foreach ($this->roles as $rol){
            $roles[] = $rol->display_name;
        }
        return implode(", ", $roles);
    }

    /**
     * Scope a query to match users by upper(name).
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $column
     * @param string $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLikeUpper($query, $column, $value)
    {
        if(!is_null($value)){
            return $query->where(DB::raw('upper('.$column.')'),'LIKE', '%'.strtoupper($value).'%');
        }else{
            return $query;
        }
    }

    /**
     * Scope a query to match users who has a role.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $role_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasRole($query, $role_id)
    {
        if(!is_null($role_id)){
            return $query->whereHas('roles', function ($q) use($role_id) {
                $q->where('id', '=', $role_id);
            });
        }else{
            return $query;
        }
    }
    
}