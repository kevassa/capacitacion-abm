@extends('backend.layouts.master')

@section('title', 'Editando Usuario')

@section('content')

<h3>Editando {{ $user->username }}</h3>


{!! Form::model($user, array('route' => array('backend.user.update', $user->id), 'class' => 'form-horizontal', 'id'=>'create_form')) !!}
    @include('backend/user/shared/form', array('user' => $user))
    <input type="hidden" id="editando" />
{!! Form::close() !!}

@endsection
