@extends('backend.layouts.master')

@section('title', 'Listado de Contenidos')

@section('content')

{{-- TODO: Layout para el h1? --}}
<h1>Contenidos</h1>
<h3>Listado de Contenidos</h3>

@include('backend/shared/alerts/messages')

<div class="well">
        <form class="form-inline">   
            <div class="row">
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('nombre', 'Nombre', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('nombre', null, array('class' => 'form-control col-sm-6', 'style' => 'width:200px;', 'id'=>'nombre'))  !!}
                     </div>        
                   </div>
                </div>
                <div class="col-sm-5">
                	<div class="form-group">
                     {!! Form::label('tipo', 'Tipo de Contenido', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::select('tipo', $tipos,null , array('class' => 'form-control col-sm-6', 'style' => 'width:250px;' , 'placeholder' => 'Todos' , 'id'=>'tipo'))  !!}
                     </div>        
                   </div>
                </div>
            </div>
            <br/>
            
            <div style="text-align:right;">
                <br/>
                <button type="button" id="botonBuscar" class="btn btn-info">Buscar</button>        
                <button type="button" onclick="$('.form-control').val('');" class="btn btn-primary">Limpiar</button>        
            </div>       
        </form>
</div>

<div class="pull-right">
    @permission('CREAR_ROL')
    <a href="{{route('backend.nodos.add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
    @endpermission
</div>
<br/><br/>
<div style="padding-right:15px; padding-bottom:20px">
    <table class="table table-bordered table-striped table-condensed" id="tableNodos" >
        <thead>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Fecha Creación</th>
            <th>Fecha Ult. Modificación</th>
            <th width="80px">Acciones</th>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<!-- Solicitar siempre confirmación -->
<div class="modal fade" id="confirmarEliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Confirmaci&oacute;n</h4>
        </div>
        <div class="modal-body">
            <h3 id='lblEliminar'></h3>
            Una vez eliminado sus datos no se visualizaran en el sistema
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" id="butConfirmarEliminar">Aceptar</button>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    //**************
    //Inicialización
    //**************
    $(document).ready(function(){
        $('#botonBuscar').click();
    });
    
    //*****************
    //Eventos Generales
    //*****************
    
    //Prevenir submit por enter
    $(window).keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        $('#botonBuscar').click();
        return false;
      }
    }); 
    
    //*********
    //Datatable
    //*********
    $('#botonBuscar').click(function()
    {
       $('#tableNodos').find('tbody').empty(); //destruyo la tabla existente, si hay que hacer una nueva búsqueda
       $('#tableNodos').find('tbody').hide(); //oculto mientras carga
       $('#tableNodos_paginate').remove();
       $('#cargando').modal('show');
       $('#tableNodos').DataTable(
       { //lleno la tabla usando DataTables
          "destroy": true,
          "paging": true,
          "ordering": false,
          "lengthChange": true,
          "searching": false,
          "info": true,
          "serverSide": true,
          "language":
          {
             "sProcessing": "Procesando...",
             "sZeroRecords": "No se encontraron resultados",
             "sEmptyTable": "No se encuentra ningún dato disponible en esta tabla",
             "sLoadingRecords": "Cargando...",
             "sLengthMenu": "Mostrar _MENU_ filas por página",
             "info": "_START_-_END_ de _TOTAL_ ",
             "oPaginate":
             {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
             }
          },
          "ajax":
          {
             url: "{{route('backend.nodos.datatable')}}",
             headers:
             {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
             },
             data:
             {
                "nombre": $("#nombre").val(),
                "tipo":$("#tipo").val()
             },
             type: 'POST',
             dataSrc: 'data'
          },
          "columns": [
            {
               render: function(data, type, row)
               {
                  //Nombre
                  return row[0];
               }
            },
            {
               render: function(data, type, row)
               {
                  //Tipo
                  return row[1];
               }
            },
            {
               render: function(data, type, row)
               {
                  //Fecha Creacion
                  return row[2];
               }
            },
            {
               render: function(data, type, row)
               {
                  //Fecha Ultima Modificacion
                  return row[3];
               }
            },
            {
               render: function(data, type, row)
               {
                  //Botones de borrado y edicion
                  id = row[4];
                  nombre = row[0];
                  urlEdicion = "{{route('backend.nodos.edit')}}".replace(/%7Bid%7D/g,id);
                  urlBorrado = "{{route('backend.nodos.delete')}}".replace(/%7Bid%7D/g,id);
                  
                  formBorrar  = "<form method='POST' action='"+urlBorrado+"' accept-charset='UTF-8' class='form-horizontal'>";
                  tokenForm = '{!! csrf_field() !!}';
                  botonBorrar = "<a href='#' class='btn btn-danger frmEliminar' data-toggle='tooltip' data-placement='bottom' data-nodo-display_name='"+nombre+"' title='Eliminar "+nombre+"'>\n\
                                    <i class='fa fa-eraser'></i>\n\
                                </a>";
                  botonEditar = "<a style='margin-left:10px' href='"+urlEdicion+"' class='btn btn-info' data-toggle='tooltip' data-placement='bottom' title='Editar "+nombre+"'>\n\
                                    <i class='fa fa-edit'></i>\n\
                                </a>"
                                        
                  return ('<center>'+formBorrar + tokenForm + botonBorrar + botonEditar + "</form></center>");
               }
            }
            ],
            "pageLength": 10,
            "initComplete" : function(settings, json) {
                $('#tableNodos').find('tbody').show(); //cuando termina de cargar muestro la tabla
                $('#cargando').modal('hide');
            }
        });
      });

    //***********
    //Eliminacion
    //***********
    var eliminarForm;
    $('#tableNodos').on("click",".frmEliminar",function () {
        var descripcion = $(this).attr('data-nodo-display_name');

        eliminarForm = $(this).parent();
        $('#lblEliminar').html('¿Confirma que desea eliminar ' + descripcion + '?');
        $('#confirmarEliminar').modal('show');

    });

    $('#butConfirmarEliminar').click(function () {
        eliminarForm.submit();
    });

</script>

@endsection
