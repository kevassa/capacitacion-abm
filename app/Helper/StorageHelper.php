<?php

namespace App\Helper;

use Storage;

/**
 * Description of StorageHelper
 *
 * @author usuario
 */
class StorageHelper {
    
    const file_path = "inscription_%u/";

    public static function save_file($inscription_id, $file_name, $file){
        
        $path = sprintf(static::file_path, $inscription_id) . $file_name;
        Storage::put($path, file_get_contents($file->getRealPath()));
        
    }
    
    public static function save_file_complemento($inscription_id, $complemento_id,$file_name, $file){
        
        $path = sprintf(static::file_path, $inscription_id)."complemento_".$complemento_id ."/". $file_name;
        Storage::put($path, file_get_contents($file->getRealPath()));
        
    }
    
    public static function delete_file($inscription_id, $file_name){
        
        $path = sprintf(static::file_path, $inscription_id) . $file_name;
        if(Storage::exists($path))
            Storage::delete($path);
        
    }
    
    public static function delete_file_complemento($inscription_id,$complemento_id, $file_name){
        
        $path = sprintf(static::file_path, $inscription_id)."complemento_".$complemento_id ."/". $file_name;
        if(Storage::exists($path))
            Storage::delete($path);
        
    }
    
    public static function get_file($inscription_id, $file_name){
        
        $path = sprintf(static::file_path, $inscription_id) . $file_name;
        return Storage::get($path);
        
    }
    
    public static function get_file_path($inscription_id, $file_name){
        
        return storage_path('app/' . sprintf(static::file_path, $inscription_id) . $file_name);
        
    }
    
    public static function get_mime_type($inscription_id, $file_name){
        return Storage::mimeType(sprintf(static::file_path, $inscription_id) . $file_name);
    }
    
}
