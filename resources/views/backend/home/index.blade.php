@extends('backend.layouts.master') 
@section('content')
    @include('backend/shared/alerts/messages')
    <div class="jumbotron" style="background:white">
        <center>
        <div class="container" style="text-align:left">
            <span style="font-size:44px">¡Bienvenido!</span>
            <br><br>
            <span style="font-size:20px">Accesos rápidos:</span>
            <br><br>
            <div style="margin-left:30px">
                <a href="{{route('backend.nodos.add')}}" style="font-size:20px">Nuevo contenido</a>
                <br>
                <a href="{{route('backend.user')}}" style="font-size:20px">Listado de usuarios</a>
            </div>
        </div>
        </center>
    </div>
@endsection