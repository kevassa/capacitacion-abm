<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\NodoNavegacion;

use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

use DB;

class NodosNavegacionController extends Controller
{   
    /*
     * TODO: Permisologia
     */
    //********************
    //Parámetros generales
    //********************
    private $tipos = [1 => "Enlace",
                      2 => "Página"];
    //***********
    //ABM Basicos
    //***********
    //<editor-fold> 
    public function index(Request $request)
    {
        return view('backend/nodo/list')
                ->with('tipos',$this->tipos);
    }
    
    public function create()
    {
        $padres = NodoNavegacion::getNodosPadres()->lists('nombre','id');
        return view ('backend/nodo/create')
                    ->with('id', 'NULL')
                    ->with('padres',$padres)
                    ->with('tipos', $this->tipos);
    }


    public function store(Request $request)
    {
        $data = $request->input();
        $v = Validator::make($data, NodoNavegacion::rules());
        if ($v->fails()){
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $r = NodoNavegacion::create($data);
        
        
        return redirect()->route('backend.nodos')
                ->with('message', trans('abms_core.created', ['name' => 'nodo']));
    }
    
    public function edit($id)
    {
        $nodo = NodoNavegacion::findOrFail($id);
        $padres = NodoNavegacion::getNodosPadres()->lists('nombre','id');
        return view ('backend/nodo/edit')
                ->with('id', $id)
        	->with('nodo', $nodo)
                ->with('padres',$padres)
                ->with('tipos', $this->tipos)
        ;
    }
    
    public function update(Request $request, $id)
    {
        $data = $request->input();
        
        $nodo = Role::findOrFail($id);
        $v = Validator::make($data, Role::rules($data['id']));
        if ($v->fails()){
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $nodo->update($data);
        
        if(isset($data['perms']))
        $nodo->savePermissions($data['perms']);
        
        return redirect()->route('backend.nodo')
                ->with('message', trans('abms_core.updated', ['name' => 'nodo']));
    }

    public function destroy($id)
    {
        $nodo = NodoNavegacion::findOrFail($id);
        
        $nodo->delete();
        
        return redirect()->route('backend.nodos')
                ->with('message', trans('abms_core.deleted', ['name' => 'nodo']));
    }
    //</editor-fold> 
    
    //****
    //AJAX
    //****
    //<editor-fold> 
    
    public function validateInput(Request $request){
        $data = $request->input();
        $esEnlace = $data['tipo'] == 1;
        $v = Validator::make($data, NodoNavegacion::rules($esEnlace,$data['id']));
        
        if ($v->fails()){
           return response()->json(['estado' => "error", 'mensajes' => $v->errors()]);
        }
        return response()->json(['estado' => "OK", 'mensajes' => []]);
    }
    
    public function renderDatatable(Request $request){
        $data = $request->input();
        $query = NodoNavegacion::getListado($data);
        return Datatables::of($query)->make();
    }
    
    //</editor-fold> 
}
