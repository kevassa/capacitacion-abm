<div class="col-sm-12" style="border-bottom:1px solid #cecece">
    <img src="<?php echo $message->embed($pathLogoLPA); ?>" 
         alt="Leer Para Aprender" class="image img-responsive" 
         style="max-width:270px; max-height:270px" >
    <span style="font-size:25px;line-height:21px;color:#141823">
        Programa Leer Para Aprender
    </span>
    <br><br>
</div>
<div style="font-size: 14.0pt; padding-left:100px">
    <br> <br>
    <br><br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Gracias por participar. 
    </span>
    <br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Con este usuario y contraseña podrán completar todos los pasos siguientes del Proceso de Selección de Libros de Texto. 
    </span>
    <br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Link de acceso a la Plataforma: {{route('backend.login')}}
    </span>
    <br><br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Tus datos de acceso son
    </span>
    <br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Usuario: {{$email}}
    </span>
    <br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Contraseña: {{$pass}}
    </span>
    <br><br>
</div>
<img src="<?php echo $message->embed($pathLogoMinisterio); ?>" 
         alt="Leer Para Aprender" class="image img-responsive" 
         style="max-width:350px; max-height:350px" >