<div class="form-group">
        {!! Form::label('oferente', 'Editorial', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[oferente]', $user->editorial->oferente, array('class' => 'form-control', 'onkeypress' => 'return textOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('cuit', 'CUIT', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[cuit]', $user->editorial->cuit, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('domicilio', 'Domicilio', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[domicilio]', $user->editorial->domicilio, array('class' => 'form-control',  'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('localidad', 'Localidad', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[localidad]', $user->editorial->localidad, array('class' => 'form-control',  'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('telefono', 'Teléfono', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[telefono]', $user->editorial->telefono, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Correo Electrónico', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[email]', $user->editorial->email, array('class' => 'form-control', 'id'=>'email', 'style' =>"text-transform:uppercase" , 'disabled' =>"disabled"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('isbn', 'Código/s de Editorial para ISBN', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[isbn]', $user->editorial->isbn, array('class' => 'form-control', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('representante_legal', 'Nombre del Representante Legal', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[representante_legal]', $user->editorial->representante_legal, array('class' => 'form-control', 'onkeypress' => 'return textOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('isbn', 'DNI', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[dni]', $user->editorial->dni, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('isbn', 'Código Postal', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[codigo_postal]', $user->editorial->codigo_postal, array('class' => 'form-control', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('isbn', 'Número de Piso', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[numero_piso]', $user->editorial->numero_piso, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('isbn', 'Número de Oficina', array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('editorial[numero_oficina]', $user->editorial->numero_oficina, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>