<style>
  body.modal-open-noscroll {
    margin-right: 0!important;
    overflow: hidden;
  }
  .modal-open-noscroll .navbar-fixed-top, .modal-open .navbar-fixed-bottom {
    margin-right: 0!important;
  }
</style>
@include('frontend.inscripcion.shared.errores')
<div class="col-sm-12">
    <div class="form-group">
        <label for="oferente" class='col-sm-3 control-label'>Editorial <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('oferente', null, array('class' => 'form-control', 'onkeypress' => 'return textOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="cuit" class='col-sm-3 control-label'>CUIT <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('cuit', null, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="domicilio" class='col-sm-3 control-label'>Domicilio <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('domicilio', null, array('class' => 'form-control',  'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="localidad" class='col-sm-3 control-label'>Localidad <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('localidad', null, array('class' => 'form-control',  'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="telefono" class='col-sm-3 control-label'>Teléfono <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('telefono', null, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="email" class='col-sm-3 control-label'>Correo Electrónico <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('email', null, array('class' => 'form-control', 'id'=>'email', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="email" class='col-sm-3 control-label'>Repita Correo Electrónico <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('email2', null, array('class' => 'form-control', 'id'=>'email2', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="isbn" class='col-sm-3 control-label'>Código/s de Editorial para ISBN <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('isbn', null, array('class' => 'form-control', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="representante_legal" class='col-sm-3 control-label'>Nombre del Representante Legal <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('representante_legal', null, array('class' => 'form-control', 'onkeypress' => 'return textOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label for="dni" class='col-sm-3 control-label'>DNI <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('dni', null, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label class='col-sm-3 control-label'>Código Postal <span style="color: red">*</span></label>
        <div class="col-sm-6">
            {!! Form::text('codigo_postal', null, array('class' => 'form-control', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label class='col-sm-3 control-label'>Número de Piso </label>
        <div class="col-sm-6">
            {!! Form::text('numero_piso', null, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <div class="form-group">
        <label class='col-sm-3 control-label'>Número de Oficina </label>
        <div class="col-sm-6">
            {!! Form::text('numero_oficina', null, array('class' => 'form-control', 'onkeypress' => 'return numberOnly(event)', 'style' =>"text-transform:uppercase"))  !!}
        </div>
    </div>
    <br>
    <center>
        <div class="alert alert-info">
            Al inscribirse, se le enviará un correo electrónico con sus datos de acceso al portal de carga. 
            <br>
            Recuerde imprimir el pdf adjunto, firmarlo y presentarlo en la UEN en el momento de la Presentación de Muestras.
        </div>
    </center>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <button type="button" id="submit_btn" name="submit_btn" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Inscribirse</button>
        </div>
    </div>
</div>


<div class='modal fade' id="cargando"  data-keyboard="false" data-backdrop="static" >
    <div class="modal-dialog modal-lg" role="document" style="padding-top:10%">
        <div class="modal-content" >
            <div class="modal-body" style="text-align:center">
                <h2><img src="frontend/images/loader.gif" style='height:75px;width:75px' /> <b>Un momento...<b></h2>
            </div>
        </div>
    </div>
</div>
<script>
    //Fix de modals
    $('.modal').on('show.bs.modal', function () {
      if ($(document).height() > $(window).height()) {
        // no-scroll
        $('body').addClass("modal-open-noscroll");
      }
      else { 
        $('body').removeClass("modal-open-noscroll");
      }
    })
    $('.modal').on('hide.bs.modal', function () {
        $('body').removeClass("modal-open-noscroll");
    })
    
    function numberOnly(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode==8 | charCode==46) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    
    function textOnly(evt){
        var keyCode = (evt.which) ? evt.which : evt.keyCode
        if (keyCode==8 | keyCode==46)return true;
        if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 122) && keyCode != 32 && keyCode!=241 && keyCode!=209)
            return false;
        return true;
    }
var displayErrors = function (errors) {
    $("#error-alert").empty();
    $('#error-ul').empty();
    $.each(errors, function () {
        $('#error-ul').append("<li>" + this + "</li>");
    });
    $("#error-alert").html($("#error-template").html());
    $("html, body").animate({scrollTop: 0});   
}
$('#submit_btn').click(function(){
    $('#cargando').modal('show');
    $.ajax({  
        type: 'POST',
        url : "{{action('Frontend\InscripcionController@validateInscripcion')}}",
        async : true,
        data: $('#create_form').serialize(),
        dataType: 'JSON',
        success: function(data) {
            var errores = $.map(data.mensajes, function(value, index){
                    return [value];
                });
            if(($('#email').val().toLowerCase()!==$('#email2').val().toLowerCase())){ 
                errores.push("Falló la confirmación. Verifique que los correos electrónicos sean iguales.");
            }
          
            if(errores.length == 0){
                $('#create_form').submit(); 
            }
            else{
                $('#cargando').modal('hide');
                displayErrors(errores);
                $('html, body').animate({ scrollTop: 0 }, 'fast');                   
            }
        },
        error: function(data){
            $('#cargando').modal('hide');
            window.alert("FAIL");
        }
    });
});    
</script>