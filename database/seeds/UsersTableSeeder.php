<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Provincia;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $username = 'admin';
        $u = User::where('username', '=', $username)->first();
        if(!$u){            
            $u = new User();
            $u->password = \Hash::make('admin');
        }
        
        $u->username = $username;
        $u->name = "Administrador";
        $u->lastname = "Administrador";
        $u->save();
        
        
        $roleAdmin = Role::where('name', '=', Role::SUPERADMIN)->first();
        
        if(!$u->hasRole($roleAdmin->name)){
            $u->attachRole($roleAdmin);
        }
    }
    
    
}
