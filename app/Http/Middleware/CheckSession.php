<?php

namespace App\Http\Middleware;

use Closure;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session = $request->session()->all();
        $logtime = isset($session["logtime"])? $session["logtime"] : null;
        if( isset($logtime)){
            
            $timeout = env('SESSION_TIMEOUT', 60);
            $minutosLogeado = $logtime->diffInMinutes(\Carbon\Carbon::now());
            if($minutosLogeado >= $timeout){
                \Session::flush();
                \Auth::logout();
                \Session::flash('message', "Su sesión ha expirado. Por favor, ingrese nuevamente");
                \Session::flash('type', 'error');
            }
        }
        return $next($request);
    }
}
