var elixir = require('laravel-elixir');
require('laravel-elixir-helpers');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    
    /*JQUERY*/
    mix.copy('node_modules/jquery/dist/jquery.js',                                              'resources/assets/js/backend/jquery.js');
    
    mix.copy('node_modules/jquery/dist/jquery.js',                                              'resources/assets/js/frontend/jquery.js');
    
    /*BOOTSTRAP 3*/
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.css',                                   'resources/assets/css/backend/bootstrap.css');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.js',                                     'resources/assets/js/backend/bootstrap.js');
    mix.copy('node_modules/bootstrap/fonts',                                                    'resources/assets/fonts/backend/');
    
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.css',                                   'resources/assets/css/frontend/bootstrap.css');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.js',                                     'resources/assets/js/frontend/bootstrap.js');
    mix.copy('node_modules/bootstrap/fonts',                                                    'resources/assets/fonts/frontend/');
    
    /*FONT AWESOME*/
    mix.copy('node_modules/font-awesome/fonts',                                                 'resources/assets/fonts/backend/');
    mix.copy('node_modules/font-awesome/css/font-awesome.css',                                  'resources/assets/css/backend/font-awesome.css');
    
    mix.copy('node_modules/font-awesome/fonts',                                                 'resources/assets/fonts/frontend/');
    mix.copy('node_modules/font-awesome/css/font-awesome.css',                                  'resources/assets/css/frontend/font-awesome.css');
    
    /*CHOSEN JS*/
    mix.copy('node_modules/chosen-js/chosen.jquery.js',                                         'resources/assets/js/backend/chosen.jquery.js');
    mix.copy('node_modules/chosen-js/chosen.css',                                               'resources/assets/css/backend/chosen.css');
    
    mix.copy('node_modules/chosen-js/chosen-sprite.png',                                        'public/backend/images/chosen-sprite.png');
    mix.copy('node_modules/chosen-js/chosen-sprite@2x.png',                                     'public/backend/images/chosen-sprite@2x.png');
    
    mix.copy('node_modules/chosen-js/chosen.jquery.js',                                         'resources/assets/js/frontend/chosen.jquery.js');
    mix.copy('node_modules/chosen-js/chosen.css',                                               'resources/assets/css/frontend/chosen.css');
    
    mix.copy('node_modules/chosen-js/chosen-sprite.png',                                        'public/frontend/images/chosen-sprite.png');
    mix.copy('node_modules/chosen-js/chosen-sprite@2x.png',                                     'public/frontend/images/chosen-sprite@2x.png');
	
    mix.copy('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',           'resources/assets/js/frontend/bootstrap-datepicker.min.js');
    mix.copy('node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js',   'resources/assets/js/frontend/bootstrap-datepicker.es.min.js');
    mix.copy('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',        'resources/assets/css/frontend/bootstrap-datepicker3.min.css');
    
    mix.copy('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',           'resources/assets/js/backend/bootstrap-datepicker.min.js');
    mix.copy('node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js',   'resources/assets/js/backend/bootstrap-datepicker.es.min.js');
    mix.copy('node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css',        'resources/assets/css/backend/bootstrap-datepicker3.min.css');
        
    mix.copy('node_modules/educar-bootstrap-theme/dist/img',                                    'public/build/css/img');
    
    //Resumable JS
    mix.copy('node_modules/resumablejs/resumable.js',                                      'resources/assets/js/backend/resumable.js');
    mix.copy('fileinput.min.js',                                                                 'resources/assets/js/frontend/fileinput.min.js');
   
   //CKEDITOR
   mix.copy('node_modules/ckeditor', 'public/backend/js/ckeditor');
   
   //CKEDITOR Plugins
   mix.copy('resources/assets/js/ckeditorPlugins/html5audio', 'public/backend/js/ckeditor/plugins/html5audio');
   mix.copy('resources/assets/js/ckeditorPlugins/html5video', 'public/backend/js/ckeditor/plugins/html5video');
   
   //Datatables
   /*DATATABLE*/
    
    mix.copy('node_modules/datatables.net/js/jquery.dataTables.js',           'resources/assets/js/backend/jquery.dataTables.js');
    mix.copy('node_modules/datatables.net-bs/css/dataTables.bootstrap.css',           'resources/assets/css/backend/dataTables.bootstrap.css');
    mix.copy('node_modules/datatables.net-bs/js/dataTables.bootstrap.js',           'resources/assets/js/backend/dataTables.bootstrap.js');
});

elixir(function(mix) {
    mix.sass([
        'app.scss'
    ], 'resources/assets/css/frontend/');
    mix.sass([
    	'backend.scss'
    ], 'resources/assets/css/backend');
});

elixir(function(mix) {
    
   mix.urlAdjuster('resources/assets/css/backend/chosen.css', {
        prependRelative: '/backend/images/'
    }, 'resources/assets/css/backend');
    
    mix.urlAdjuster('resources/assets/css/backend/app.css', {
    	replace:  ['../fonts/bootstrap','/fonts'],
    }, 'resources/assets/css');
    
    mix.urlAdjuster('resources/assets/css/frontend/app.css', {
    	replace:  ['../fonts/bootstrap','/frontend/fonts'],
    }, 'resources/assets/css/frontend');
    
    mix.urlAdjuster('resources/assets/css/frontend/app.css', {
    	replace: ['../img', '/frontend/images'],
    }, 'resources/assets/css/frontend');
    
    elixir(function(mix) {
        mix.copy('resources/assets/fonts/frontend', 'public/frontend/fonts');
        mix.copy('public/frontend/fonts', 'public/build/frontend/fonts');

        mix.copy('resources/assets/fonts/backend', 'public/backend/fonts');
        mix.copy('public/backend/fonts', 'public/build/backend/fonts');
        mix.copy('resources/assets/css/backend', 'public/backend/css');
        mix.copy('public/backend/css', 'public/build/backend/css');
    });
    
    mix.styles([
    	/*"backend/bootstrap.css",*/
        "backend/app.css",
    	"backend/font-awesome.css",
    	"backend/chosen.css",        
    	"backend/bootstrap-datepicker3.min.css",
        'backend/dataTables.bootstrap.css'
    ], "public/backend/css");
    
    mix.styles([
    	"frontend/app.css",
    	"frontend/font-awesome.css",
    	"frontend/chosen.css",
    	"frontend/bootstrap-datepicker3.min.css"
    ], "public/frontend/css");
    
});

elixir(function(mix) {
    mix.copy('resources/assets/fonts/frontend', 'public/fonts/frontend');
    mix.copy('public/frontend/fonts', 'public/build/fonts/frontend');
});

elixir(function(mix) {
    mix.copy('resources/assets/fonts/backend', 'public/fonts/backend');
    mix.copy('public/backend/fonts', 'public/build/fonts/backend');
});

elixir(function(mix) {
    mix.scripts([
        'backend/jquery.js',
        'backend/bootstrap.js',
        'backend/chosen.jquery.js',
        'backend/bootstrap-datepicker.min.js',
        'backend/bootstrap-datepicker.es.min.js',
       'backend/resumable.js',
       'backend/jquery.dataTables.js',
        'backend/dataTables.bootstrap.js'
    ], 'public/backend/js');
});

elixir(function(mix) {
    mix.scripts([
        'frontend/jquery.js',
        'frontend/bootstrap.js',
        'frontend/chosen.jquery.js',
        'frontend/Helpers/Helpers.js',
        'frontend/Helpers/AnaliticsEvents.js',
        'frontend/IframeHeight/IframeHeight.js',
        'frontend/DeviceInfo/DeviceInfo.js',
        'frontend/CascadeDropdown/CascadeDropdown.js',
        'frontend/ResponsiveImages/ResponsiveImages.js',
        'frontend/Clamp/ClampWrapper.js',
        'frontend/SearchFilters/SearchFilters.js',
        'frontend/Clamp/Clamp.js',
        'frontend/AdjustDescriptionHeight/AdjustDescriptionHeight.js',
        'frontend/Playlist/Playlist.js',
        'frontend/Playlist/VideoIframe.js',
        'frontend/AudioPlayer/AudioPlayer.js',
        'frontend/bootstrap-datepicker.min.js',
        'frontend/bootstrap-datepicker.es.min.js',
        'frontend/fileinput.min.js'
    ], 'public/frontend/js');
});


elixir(function(mix) {
    mix.version(["public/backend/css/all.css", "public/backend/js/all.js", "public/frontend/css/all.css", "public/frontend/js/all.js"]);
});