<div class="col-sm-12" style="border-bottom:1px solid #cecece">
    <img src="<?php echo $message->embed($pathToImage); ?>" 
         alt="Leer Para Aprender" class="image img-responsive" 
         style="max-width:270px; max-height:270px" >
    <span style="font-size:25px;line-height:21px;color:#141823">
        Programa Leer Para Aprender
    </span>
    <br><br>
</div>
<div style="font-size: 14.0pt; padding-left:100px">
    <br> <br>
    <span style="font-size:30px;line-height:21px;color:#141823">
        ¡{{$name}}, tu contraseña fue restaurada!
    </span>
    <br><br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Puedes ingresar al sistema <a href="{{route('backend.login')}}">haciendo click aqui</a>.
    </span>
    <br><br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Tus datos de acceso son
    </span>
    <br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Usuario: {{$email}}
    </span>
    <br>
    <span style="font-size:16px;line-height:21px;color:#141823">
        Contraseña: {{$pass}}
    </span>
</div>