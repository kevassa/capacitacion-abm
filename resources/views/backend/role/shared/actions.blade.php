{!! Form::model($role, ['route' => ['backend.role.delete', $role->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@permission('ELIMINAR_ROL')
<a href="#" class="btn btn-danger frmEliminar" data-toggle="tooltip" data-placement="bottom" data-role-display_name='{{ $role->display_name }}' data-role-id='{{ $role->id }}' title="Eliminar {{ $role->display_name }}">
    <i class="fa fa-eraser"></i>
</a>
@endpermission
@permission('EDITAR_ROL')
<a href="{{route('backend.role.edit', ['id' => $role->id])}}" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Editar {{ $role->display_name }}">
    <i class="fa fa-edit"></i>
</a>
@endpermission
{!! Form::close() !!}
