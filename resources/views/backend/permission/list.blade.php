@extends('backend.layouts.master')

@section('title', 'Listado de Permisos')

@section('content')

<h3>Listado de Permisos</h3>

@include('backend/shared/alerts/messages')

<div class="well">
        <form class="form-inline">   
            <div class="row">
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('name', 'Nombre', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('name', $name, array('class' => 'form-control col-sm-6', 'id' => 'name', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('display_name', 'Display Name', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('display_name', $display_name, array('class' => 'form-control col-sm-6', 'id' => 'display_name', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('description', 'Descripcion', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('description', $description, array('class' => 'form-control col-sm-6', 'id' => 'description', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
            </div>
            <br/>
            
            <div style="text-align:right;">
                <br/>
                <button type="submit" class="btn btn-info">Buscar</button>        
                <button type="button" onclick="$('#name').val('');$('#display_name').val('');$('#description').val('');" class="btn btn-primary">Limpiar</button>        
            </div>       
        </form>
</div>

<div class="pull-right">
	<a href="{{route('backend.permission.xls')}}" class="btn btn-primary fa fa-print" title="Exportar datos a XLS"> XLS</a>
</div>
<br/><br/>

<table class="table table-bordered table-striped table-condensed">
    <thead>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('id', 'ID') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('name', 'Nombre') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('display_name', 'Nombre Mostrado') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('description', 'Descripción') !!}</th>
    </thead>
    <tbody>
        
        @foreach($permissions as $p)
            <tr>
                <td>{{ $p->id }}</td>
                <td>{{ $p->name }}</td>
                <td>{{ $p->display_name }}</td>
                <td>{{ $p->description }}</td>
            </tr>

        @endforeach
        @if(count($permissions)==0)
            <tr>
                <td colspan="4">
                    <center>No se han encontrado resultados para la búsqueda.</center>
                </td>
            </tr>
        @endif
    </tbody>
</table>

<script type="text/javascript">
    
</script>

{!! $permissions->appends(Input::except('page'))->render() !!}
@endsection
