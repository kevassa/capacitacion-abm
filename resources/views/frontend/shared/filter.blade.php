@if(sizeof($filters) > 1)
    <?php $extras_params = isset($extras_params)?$extras_params:[]; ?>

    <div class="filter-group">
        <h4>{{$title}}</h4>
        <ul class="list-group">
            @foreach($filters as $index => $f)
                <li class="list-group-item @if($index >= 4)hidden @endif"><a class="js-filter" href="{{route($url, request()->except('page') + array($key => $f->id) + $extras_params)}}"><span class="badge">{{$f->total or 0}}</span>{{$f->description or $f->name}}</a></li>
            @endforeach
            <li class="js-more-tags @if(count($filters) <= 4)hidden @endif"><a href="#">más opciones <span class="glyphicon glyphicon-chevron-down"></span></a></li>
        </ul>
    </div>
@endif