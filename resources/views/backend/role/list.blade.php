@extends('backend.layouts.master')

@section('title', 'Listado de Roles')

@section('content')

<h3>Listado de Roles</h3>

@include('backend/shared/alerts/messages')

<div class="well">
        <form class="form-inline">   
            <div class="row">
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('name', 'Nombre', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('name', $name, array('class' => 'form-control col-sm-6', 'id' => 'name', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
                <div class="col-sm-5">
                	<div class="form-group">
                     {!! Form::label('display_name', 'Display Name', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('display_name', $display_name, array('class' => 'form-control col-sm-6', 'id' => 'display_name', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('description', 'Descripcion', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::text('description', $description, array('class' => 'form-control col-sm-6', 'id' => 'description', 'style' => 'width:200px;'))  !!}
                     </div>        
                   </div>
                </div>
                <div class="col-sm-5">  
                   <div class="form-group">
                     {!! Form::label('perm_id', 'Permiso', array('class' => 'col-sm-4 control-label', 'style' => 'width:150px;')) !!}
                     <div class="col-sm-4">
                        {!! Form::select('perm_id', $permissions, $perm_id, ['class' => 'form-control col-sm-6', 'style' => 'width:200px;']) !!}
                     </div>        
                   </div>
                </div>
            </div>
            <br/>
            
            <div style="text-align:right;">
                <br/>
                <button type="submit" class="btn btn-info">Buscar</button>        
                <button type="button" onclick="$('#name').val('');$('#display_name').val('');$('#description').val('');$('#perm_id').val('0');" class="btn btn-primary">Limpiar</button>        
            </div>       
        </form>
</div>

<div class="pull-right">
	<a href="{{route('backend.role.xls')}}" class="btn btn-primary fa fa-print" title="Exportar datos a XLS"> XLS</a>
    @permission('CREAR_ROL')
        <a href="{{route('backend.role.add')}}" class="btn btn-primary fa fa-plus"> Agregar</a>
    @endpermission
</div>
<br/><br/>

<table class="table table-bordered table-striped table-condensed">
    <thead>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('id', 'ID') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('name', 'Nombre') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('display_name', 'Nombre Mostrado ') !!}</th>
        <th>{!! \App\Traits\SortableTrait::link_to_sorting_action('description', 'Descripción') !!}</th>
        <th>Permisos</th>
        <th style='width:10%;'>Acciones</th>
    </thead>
    <tbody>
        
        @foreach($roles as $r)
            <tr>
                <td>{{ $r->id }}</td>
                <td>{{ $r->name }}</td>
                <td>{{ $r->display_name }}</td>
                <td>{{ $r->description }}</td>
                <td>{{ $r->getPermisosDescAttribute() }}</td>
                <td>@include('backend/role/shared/actions', array('role' => $r))</td>
            </tr>

        @endforeach
        @if(count($roles)==0)
            <tr>
                <td colspan="7">
                    <center>No se han encontrado resultados para la búsqueda.</center>
                </td>
            </tr>
        @endif
    </tbody>
</table>

<!-- Solicitar siempre confirmación -->
<div class="modal fade" id="confirmarEliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Confirmaci&oacute;n</h4>
        </div>
        <div class="modal-body">
            <h3 id='lblEliminar'></h3>
            Una vez eliminado sus datos no se visualizaran en el sistema
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" id="butConfirmarEliminar">Aceptar</button>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    
    var eliminarForm;
    
    $('.frmEliminar').unbind().click(function(){
        
        var descripcion = $(this).attr('data-role-display_name');
        
        eliminarForm = $(this).parent();
        
        $('#lblEliminar').html('¿Confirma que desea eliminar ' + descripcion + '?');
        $('#confirmarEliminar').modal('show');
        
    });
    
    $('#butConfirmarEliminar').click(function(){
        eliminarForm.submit();
    });
    
</script>

{!! $roles->appends(Input::except('page'))->render() !!}
@endsection
