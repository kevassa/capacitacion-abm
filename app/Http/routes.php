<?php

Route::group(['namespace' => 'Backend'], function ()
{    
    Route::get('', ['as' => 'backend.login', 'uses' => 'LoginController@index']);
    // LOGIN
    Route::group(['prefix' => 'login'], function ()
    {
        Route::get('', ['as' => 'backend.login', 'uses' => 'LoginController@index']);
        Route::post('', ['as' => 'backend.login.checkLogin', 'uses' => 'LoginController@checkLogin']);
        Route::get('/passwordRecovery', ['as' => 'backend.login.recovery', 'uses' => 'LoginController@recuperarPass']);
        Route::post('/passwordRecovery', ['as' => 'backend.login.recoverysubmit', 'uses' => 'LoginController@submitRecuperar']);
    });

    //LOGOUT
    Route::get('logout', ['as' => 'backend.logout', function () {
        \Session::flush();
        \Auth::logout();
        return redirect()->route('backend.login');
    }]);

});
Route::group(['namespace' => 'Backend', 'middleware' => 'auth'], function () {
    
    Route::get('home', ['as' => 'backend.home', function () {
        return view('backend/home/index');
    }]);
    
    Route::get('error', ['as' => 'backend.error', function () {
        return view('backend/error/error');
    }]);
    //Config
    Route::group(['prefix' => 'config', 'middleware' => 'auth'], function () {
        Route::get('edit', ['as' => 'backend.config.edit', 'uses' => 'ConfiguracionController@edit']);
        Route::post('save', ['as' => 'backend.config.save', 'uses' => 'ConfiguracionController@save']);
    });
    // USER
    Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
        
        Route::get('', ['as' => 'backend.user', 'uses' => 'UsersController@index']);
        Entrust::routeNeedsPermission("user", 'VISUALIZAR_USUARIOS', redirect()->route('backend.error'));

        Route::get('add', ['as' => 'backend.user.add', 'uses' => 'UsersController@create']);
        Entrust::routeNeedsPermission('user/add', 'CREAR_USUARIO', redirect()->route('backend.error'));
        
        Route::get('myAccount', ['as' => 'backend.user.micuenta', 'uses' => 'UsersController@miCuenta']);

        Route::post('save', ['as' => 'backend.user.save', 'uses' => 'UsersController@store']);
        Entrust::routeNeedsPermission('user/save', 'CREAR_USUARIO', redirect()->route('backend.error'));

        Route::get('edit/{id}', ['as' => 'backend.user.edit', 'uses' => 'UsersController@edit']);
        Entrust::routeNeedsPermission('user/edit/*', 'EDITAR_USUARIO', redirect()->route('backend.error'));

        Route::post('validate', ['as' => 'backend.user.validate', 'uses' => 'UsersController@validateInput']);

        Route::post('update/{id}', ['as' => 'backend.user.update', 'uses' => 'UsersController@update']);
        
        Route::post('updateMyAccount', ['as' => 'backend.user.updatemicuenta', 'uses' => 'UsersController@updateMiCuenta']);
        
        Entrust::routeNeedsPermission('user/update/*', 'EDITAR_USUARIO', redirect()->route('backend.error'));

        Route::post('delete/{id}', ['as' => 'backend.user.delete', 'uses' => 'UsersController@destroy']);
        Entrust::routeNeedsPermission('user/delete/*', 'ELIMINAR_USUARIO', redirect()->route('backend.error'));
        
        Route::get('pdf', ['as' => 'backend.user.pdf', 'uses' => 'UsersController@pdf']);
        
        Route::get('xls', ['as' => 'backend.user.xls', 'uses' => 'UsersController@xls']);
        Entrust::routeNeedsPermission('user/xls', 'VISUALIZAR_USUARIOS', redirect()->route('backend.error'));
        
    });

    // ROLE
    Route::group(['prefix' => 'role', 'middleware' => 'auth'], function () {
        
        Route::get('', ['as' => 'backend.role', 'uses' => 'RolesController@index']);
        Entrust::routeNeedsPermission('role', 'VISUALIZAR_ROLES', redirect()->route('backend.error'));

        Route::get('add', ['as' => 'backend.role.add', 'uses' => 'RolesController@create']);
        Entrust::routeNeedsPermission('role/add', 'CREAR_ROL', redirect()->route('backend.error'));

        Route::post('save', ['as' => 'backend.role.save', 'uses' => 'RolesController@store']);
        Entrust::routeNeedsPermission('role/save', 'CREAR_ROL', redirect()->route('backend.error'));

        Route::get('edit/{id}', ['as' => 'backend.role.edit', 'uses' => 'RolesController@edit']);
        Entrust::routeNeedsPermission('role/edit/*', 'EDITAR_ROL', redirect()->route('backend.error'));
        
        Route::post('validate', ['as' => 'backend.role.validate', 'uses' => 'RolesController@validateInput']);
        
        Route::post('update/{id}', ['as' => 'backend.role.update', 'uses' => 'RolesController@update']);
        Entrust::routeNeedsPermission('role/update/*', 'EDITAR_ROL', redirect()->route('backend.error'));

        Route::post('delete/{id}', ['as' => 'backend.role.delete', 'uses' => 'RolesController@destroy']);
        Entrust::routeNeedsPermission('role/delete/*', 'ELIMINAR_ROL', redirect()->route('backend.error'));

        Route::get('xls', ['as' => 'backend.role.xls', 'uses' => 'RolesController@xls']);
        Entrust::routeNeedsPermission('role/xls', 'VISUALIZAR_ROLES', redirect()->route('backend.error'));
        
    });
    
    //PERMISSION
    Route::group(['prefix' => 'permission', 'middleware' => 'auth'], function () {
        
        Route::get('', ['as' => 'backend.permission', 'uses' => 'PermissionsController@index']);
        Entrust::routeNeedsPermission('permission', 'VISUALIZAR_PERMISOS', redirect()->route('backend.error'));

        Route::get('xls', ['as' => 'backend.permission.xls', 'uses' => 'PermissionsController@xls']);
        Entrust::routeNeedsPermission('permission/xls', 'VISUALIZAR_PERMISOS', redirect()->route('backend.error'));
            
    });
    
    //Modulo de Nodos (se visualiza como Contenidos)
    Route::group(['prefix' => 'contenidos', 'middleware' => 'auth'], function () {
        Route::get('', ['as' => 'backend.nodos', 'uses' => 'NodosNavegacionController@index']);
        Route::get('add', ['as' => 'backend.nodos.add', 'uses' => 'NodosNavegacionController@create']);
        Route::post('save', ['as' => 'backend.nodos.save', 'uses' => 'NodosNavegacionController@store']);
        Route::get('edit/{id}', ['as' => 'backend.nodos.edit', 'uses' => 'NodosNavegacionController@edit']);
        Route::post('update/{id}', ['as' => 'backend.nodos.update', 'uses' => 'NodosNavegacionController@update']);
        Route::post('delete/{id}', ['as' => 'backend.nodos.delete', 'uses' => 'NodosNavegacionController@destroy']);
        //AJAX
        Route::post('validate', ['as' => 'backend.nodos.validate', 'uses' => 'NodosNavegacionController@validateInput']);
        Route::post('datatable', ['as' => 'backend.nodos.datatable', 'uses' => 'NodosNavegacionController@renderDatatable']);
    });
});

Route::group(['namespace' => 'Frontend'], function() {
    //TODO:Rutas del frontend
    //Route::get('/', ['as' => 'frontend.home', 'uses' => 'LoginController@index']);
    
    //Inscripción
    /*Route::group(['prefix' => 'inscripcion', 'as' => 'frontend.'], function() {
        
    	Route::get('/', 	['as' => 'inscripcion', 'uses' => 'InscripcionController@create']);
    	Route::post('/', ['as' => 'inscripcion.add', 'uses' => 'InscripcionController@store']);
        Route::post('/validate', ['as' => 'inscripcion.validate', 'uses' => 'InscripcionController@validateInscripcion']);
    });*/
});