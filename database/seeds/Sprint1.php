<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Sprint1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(PermissionsTableSeeder::class);
        $this->call(SuperAdminRoleSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MenuPrincipalSeeder::class);
        
        Model::reguard();
    }
}
