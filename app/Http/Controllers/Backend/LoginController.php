<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Editorial;
use App\Models\User;

class LoginController extends Controller
{

    public function index()
    {
        if (\Auth::check()) {
            return view('backend/home/index');
        }

        return view('backend/login/index')->with('username', \Session::get('username'));
    }

    public function checkLogin(Request $request){
        $data = $request->input();

        $username = $data['username'];
        $password = $data['password'];

        if (\Auth::attempt(array('username' => $username, 'password' => $password), false))
        {
            \Session::put('logtime', \Carbon\Carbon::now());
            $u = User::findOrFail(\Auth::user()->id);
            $u->remember_token = null;
            $u->update();
            return view('backend/home/index');
        } else {
            return redirect()->back()
            ->with('username', $username)
            ->withErrors('Usuario o Password incorrecto')->withInput();
        }
    }
    
    public function recuperarPass(){
        return view('backend/login/recovery');
    }
    public function submitRecuperar(Request $request){
        $data= $request->input();
        $e=Editorial::where('email', '=',$data['email'])->first();
        if (!$e) return redirect()->route('backend.login')
            ->with('username', \Session::get('username'))
            ->withErrors('El mail ingresado no corresponde a ningún usuario.');
        $u=User::where('editorial_id', '=', $e->id)->first();
        $pass=$this->generateRandomString();
        $u->password=\Hash::make($pass);
        $u->save();
        \Mail::send('backend/login/shared/passRecovery',
        [   'pass' => $pass,
            'name'=> $u->name,
            'email'=>$e->email,
            'pathToImage' => public_path().DIRECTORY_SEPARATOR."frontend".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."logo-LPA.jpg"
        ],
        function ($m) use($e){
                $m->from(config('mail.from')['address'], config('mail.from')['name']);
                $m->to($e->email)->subject('¡Se restauró su contraseña!');
        });
        return redirect()->route('backend.login')
            ->with('username', \Session::get('username'))
            ->with('message','Se le ha enviado un mail con la contraseña nueva. Verifique su casilla de correo electrónico.');
    }
}