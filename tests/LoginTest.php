<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginFail()
    {
        Session::start();
        $response = $this->call('POST', '/login', [
            'username' => 'badUsername',
            'password' => 'badPass',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(302, $response->getStatusCode());
    }
    
    public function testLoginSuccessAdmin()
    {
        Session::start();
        $response = $this->call('POST', '/login', [
            'username' => 'admin',
            'password' => 'Admin#123',
            '_token' => csrf_token()
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('backend.home.index', $response->original->name());
    }
}
