<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{

    public function index(Request $request)
    {
        $data = $request->input();

        $name = isset($data['name']) ? $data['name'] : null;
        $lastname = isset($data['lastname']) ? $data['lastname'] : null;
        $username = isset($data['username']) ? $data['username'] : null;
        $role_id = isset($data['role_id']) && trim($data['role_id']) != "0" ? $data['role_id'] : null;
        $sort= isset($data['s']) ? $data['s'] : 'id';
        $dir= isset($data['o']) ? $data['o'] : 'asc';
        
         switch($sort){
            case "role":
                $users = User::select("users.*")
                    ->join("role_user", "role_user.user_id","=","users.id")
                    ->join("roles","role_user.role_id","=","roles.id")
                    ->orderBy("roles.name",$dir);
                break;
            default:
                $users=User::orderBy($sort, $dir);
                break;
        }
        $users ->likeUpper('users.name', $name)
                ->likeUpper('users.lastname', $lastname)
                ->likeUpper('users.username', $username)
                ->hasRole($role_id);

        \Session::put('usersList', $users->get());

        $roles = $this->add_default_option(Role::orderBy('display_name')->lists('display_name', 'id'));

        return view('backend/user/list')->with('users', $users->paginate())
                                                  ->with('roles', $roles)
                                                  ->with('username', $username)
                                                  ->with('name', $name)
                                                  ->with('lastname', $lastname)
                                                  ->with('role_id', $role_id)
        ;
    }

    public function xls(){
        return \Excel::create('Usuarios', function($excel) {

            $excel->sheet('Hoja 1', function($sheet) {
                $sheet->row(1, array('ID', 'USERNAME', 'NOMBRE', 'APELLIDO', 'ROL'));
                $index = 2;
                foreach (\Session::get('usersList') as $tp){
                    $sheet->row($index, array(
                                    $tp->id,
                                    $tp->username,
                                    $tp->name,
                                    $tp->lastname,
                                    $tp->rolesDesc
                    ));
                    $index++;
                }
            });

        })->download('xls');
    }

    public function create()
    {
        $roles = Role::orderBy('display_name')->get()->lists('display_name', 'id');
        
        return view ('backend/user/create')
                    ->with('roles', $roles)
                    ->with('rolesSelected', array());
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        
        $roles = Role::orderBy('display_name')->get()->lists('display_name', 'id');

        return view ('backend/user/edit')->with('user', $user)
                                         ->with('roles', $roles)
                                         ->with('rolesSelected', array_column($user->roles->toArray(), 'id'));
    }

    public function store(Request $request)
    {
        $data = $request->input();
        $v = Validator::make($data, User::rules($data['id']));
        if ($v->fails()){
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $data['password'] = \Hash::make($data['password']);
        $data['provider_id'] = \Auth::user()->provider_id;
        
        $u = User::create($data);

        $u->attachRoles($data['roles']);

        return redirect()->route('backend.user')->with('message', trans('abms_core.created', ['name' => trans('abms_core.user')]));
    }

    public function update(Request $request, $id)
    {
        $data = $request->input();
        $v = Validator::make($data, User::rules($data['id']));
        if ($v->fails()){
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $user = User::findOrFail($id);
        
        if($data['change_password'] && $data['change_password'] == 1){
            $data['password'] = \Hash::make($data['password']);
        }

        $user->update($data);
        $user->roles()->sync($data['roles']);

        return redirect()->route('backend.user')->with('message', trans('abms_core.updated', ['name' => trans('abms_core.user')]));
    }
    
    public function updateMiCuenta(Request $request)
    {
        $data = $request->input();
        
        $v = Validator::make($data, User::rules($data['id']));
        if ($v->fails()){
           return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $user = \Auth::user();
        
        if($data['change_password'] && $data['change_password'] == 1){
            $data['password'] = \Hash::make($data['password']);
        }
        $user->update($data);

        return redirect()->route('backend.home')->with('message', "Su usuario se actualizo correctamente.");
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return redirect()->route('backend.user')->with('message', trans('abms_core.deleted', ['name' => trans('abms_core.user')]));
    }
    
    public function validateInput(Request $request){
        $data = $request->input();
        $v = Validator::make($data, User::rules($data['id']));
        if ($v->fails()){
           return response()->json(['estado' => "error", 'mensajes' => $v->errors()]);
        }
        return response()->json(['estado' => "OK", 'mensajes' => []]);
    }
    
    public function miCuenta (){
        $user = \Auth::user();
        
        $roles = Role::orderBy('display_name')->get()->lists('display_name', 'id');

        return view ('backend/user/micuenta')->with('user', $user)
                                         ->with('roles', $roles)
                                         ->with('rolesSelected', array_column($user->roles->toArray(), 'id'))
                                         ->with('micuenta', "true");
    }
    
}