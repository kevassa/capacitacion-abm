<header id="main-header">
    <nav class="navbar navbar--main navbar-default" role="navigation">
        <div class="container">
            <div class="header-container">

                <div class="navbar-header" style='height:100px'>
                    <a class="navbar-brand" href="javascript:void(0);">
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sections" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="sections">
                    <ul class="nav navbar-nav">
                        <!--<li>
                            <a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/noticias" class="js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="main_nav_link" data-eventlabel="Noticias">
                                Noticias
                            </a>
                        </li>
                        <li>
                            <a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/seccion/73/que-estamos-haciendo" class="js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="main_nav_link" data-eventlabel="Qué estamos haciendo">
                                Qué estamos haciendo
                            </a>
                        </li>
                        <li>
                            <a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/seccion/256/organizacion" class="js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="main_nav_link" data-eventlabel="Organización">
                                Organización
                            </a>
                        </li>-->
                    </ul>
                    <div class="social-wrapper flex-end pull-right">
                        <a href="javascript:history.back()" class="btn btn-info pull-right">Volver</a>
                        <!--<a href="http://prod-educacion.educ.gob.ar/ministerio-de-educacion-y-deportes/contacto" class="btn-social img-circle mail js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="contacto" title="Ir a la página de contacto"><i></i></a>
                        <a href="https://twitter.com/EducacionAR" class="btn-social img-circle twitter js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="twitter" title="Ir al perfil en Twitter" target="_blank"><i></i></a>
                        <a href="https://www.facebook.com/educacionAR" class="btn-social img-circle facebook js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="facebook" title="Ir a la página de Facebook" target="_blank"><i></i></a>
                        <a href="https://www.youtube.com/user/educacionar" class="btn-social img-circle youtube js-trackingAction" data-eventcategory="Ministerio de Educación y Deportes" data-eventaction="link_social" data-eventlabel="youtube" title="Ir al perfil en YouTube" target="_blank"><i></i></a>-->
                   </div>
                </div>

            </div>
        </div>
    </nav>
</header>