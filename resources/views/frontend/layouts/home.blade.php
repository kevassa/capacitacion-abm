@extends('frontend.layouts.master') 

@section('page-title', 'Toma Carrera') 
@section('section-name', 'Inicio')

@section('central-content')
    <div class="container">
        @yield('tab-content')
    </div>
@endsection