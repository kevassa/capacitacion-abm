@extends('backend.layouts.empty')

@section('title', 'Ingreso al Sistema')

@section('content')

@include('backend/shared/alerts/errors')


<div class="container">    
        <div id="loginbox" style="margin-top:20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title" style="text-align:center;"><b>Recuperación de contraseña</b></div>
                    </div>
                <div style="padding-top:15px" class="panel-body" >
                    <div style='text-align:center'>
                    <h5>Ingrese su correo electrónico para recibir una nueva contraseña </h5>
                    </div>
                    <br>
                    <div>
                        {!! Form::open(array('route' => 'backend.login.recoverysubmit', 'method' => 'POST', 'class' => 'form-horizontal')) !!}
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-at faa-passing" style="width:15px"></i></span>                                    
                                        {!! Form::text('email', null, array('class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => 'Correo electrónico'))  !!}
                            </div>


                                <div style="margin-top:10px" class="form-group">
                                    <div class="col-sm-12 controls">
                                      <input type="submit" id="btnSubmit" value="Recuperar Contraseña" class="btn btn-primary btn-lg btn-block btn-signin" />
                                    </div>
                                </div>
                            {!! Form::close() !!}    
                        </div>  
                    </div>  
        </div>
        
    </div>

@endsection
