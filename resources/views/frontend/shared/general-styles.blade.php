<link href='https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ elixir('frontend/css/all.css') }}">
<link rel="shortcut icon" href="/images/favicon.ico">
