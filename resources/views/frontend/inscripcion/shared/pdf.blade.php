<div style="border-bottom:2px solid #cecece">
    <img src="frontend/images/logo-LPA.jpg" width="300px" height="99px">
    <br><br>
</div>
<br><br>
<center>
    <span style="font-size:19px;">FORMULARIO DE EDITORIALES </span>
</center>
<hr style="height:85px; visibility:hidden;" />
<body>
<center>
    <table class="table table-bordered" style="color:#1a1a1a; padding-left:30px; font-size:14px">
        <tr>
            <td width="80" style="text-align:left"> <b>EDITORIAL </b></td>
            <td width="320" style="padding-left:10px">{{strtoupper($e->oferente)}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"> <b>CUIT </b></td>
            <td width="320" style="padding-left:10px">{{$e->cuit}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"><b>Domicilio </b></td>
            <td width="320" style="padding-left:10px">{{strtoupper($e->domicilio)}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"><b>Localidad </b></td>
            <td width="320" style="padding-left:10px">{{strtoupper($e->localidad)}} </td>
        </tr> 
        <tr>
            <td width="80" style="text-align:left"><b>Teléfono </b></td>
            <td width="320" style="padding-left:10px">{{$e->telefono}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"><b>Correo Electrónico</b></td>
            <td width="320" style="padding-left:10px">{{strtoupper($e->email)}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"><b>Código de Editorial para ISBN </b></td>
            <td width="320" style="padding-left:10px">{{strtoupper($e->isbn)}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"> <b>Nombre del Representante Legal </b></td>
            <td width="320" style="padding-left:10px">{{strtoupper($e->representante_legal)}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"> <b>DNI </b></td>
            <td width="320" style="padding-left:10px">{{$e->dni}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"> <b>Código Postal </b></td>
            <td width="320" style="padding-left:10px">{{strtoupper($e->codigo_postal)}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"> <b>Número de Piso </b></td>
            <td width="320" style="padding-left:10px">{{$e->numero_piso}} </td>
        </tr>
        <tr>
            <td width="80" style="text-align:left"> <b>Número de Oficina </b></td>
            <td width="320" style="padding-left:10px">{{$e->numero_oficina}} </td>
        </tr>
    </table>
</center>
    <hr style="height:15%; visibility:hidden;" />
    <div style="font-size:16px; margin-left:45px">
        <table style="text-align:center">
            <tr>
                <th><span>_________________________________</span></th>
                <th><span>_________________________________</span></th>
            </tr>
            <tr>
                <td width="180" >Firma del Representante Legal </td>
                <td width="320" style="padding-left:10px">Aclaración del Representante Legal </td>
            </tr>
        </table>
    </div>
    <hr style="height:35px; visibility:hidden;" />
</body>

<style> 
.table {
    border-collapse: collapse !important; }
    .table td,
    .table th {
    height:35px}
  .table-bordered th,
  .table-bordered td {
    border: 1px solid #404040 !important; }
  
  body {
                margin: 0;
                padding: 0;
                width: 100%;
                font-weight: 500;
                line-height: 1.2;
            }
     @page { margin: 40px 50px; }
     #footer { position: fixed; left: 80%; bottom: -120px; right: 0px; height: 150px; background-color: white; }
     #footer .page:after { content: counter(page); }
</style>

