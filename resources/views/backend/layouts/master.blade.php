<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Toma Carrera</title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ elixir('backend/css/all.css') }}">
        <script type="text/javascript" src="{{ elixir('backend/js/all.js') }}"></script>
        
        <script type="text/javascript" src='/backend/js/ckeditor/ckeditor.js'></script>
        
        <style>
            
            ul.nav a:hover { 
                background-color: #35BDFF !important; 
            }
            .nav .open > a, .nav .open > a:hover, .nav .open > a:focus {
                background-color: #35BDFF !important;
            }
            
        </style>
        
    </head>
    <body>
        <div class="sidenav">
            <a href="{{route('backend.home')}}" style="font-size:21px"><b>#TomáCarrera</b></a>
            <br>
            @include('backend/menu/nodos')
            <a href="#">Usuarios</a>
            <a href="#">Reportes</a>
            @include('backend/menu/security')
        </div>
        
        <section class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-1"></div>
                        @yield('content')
                    <div class="col-md-1"></div>
                </div>
            </div>
        </section>
        <div class='modal fade' id="cargando"  data-keyboard="false" data-backdrop="static" >
            <div class="modal-dialog modal-lg" role="document" style="padding-top:10%">
                <div class="modal-content" >
                    <div class="modal-body" style="text-align:center">
                        <h2> <img src="{{asset('backend/images/loader.gif')}}" style='height:55px;width:55px' />Cargando...</h2>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        //Funciones generales
        $('select:not(.classic_select)').chosen({
            no_results_text: "Sin resultados para",
            placeholder_text_single: "Seleccione un elemento...",
            placeholder_text_multiple: "Seleccione uno o mas elementos..."
        });
            
        function numberOnly(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode==8 | charCode==46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function textOnly(evt){
            var keyCode = (evt.which) ? evt.which : evt.keyCode
            if (keyCode==8 | keyCode==46)return true;
            if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 122) && keyCode != 32 && keyCode!=241 && keyCode!=209)
                return false;
            return true;
        }
    </script>
</html>
