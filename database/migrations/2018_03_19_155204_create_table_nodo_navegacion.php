<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNodoNavegacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodos_navegacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('cuerpo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::create('nodos_padre_hijo', function (Blueprint $table) {
            $table->integer('nodo_padre_id')->unsigned();
            $table->integer('nodo_hijo_id')->unsigned();
            $table->foreign('nodo_padre_id')->references('id')->on('nodos_navegacion')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('nodo_hijo_id')->references('id')->on('nodos_navegacion')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nodos_navegacion');
        Schema::drop('nodos_padre_hijo');
    }
}
