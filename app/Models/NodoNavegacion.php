<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * Modelo de un nodo de navegacion común, se visualizan como "contenidos"
 * Un contenido enlace => nodo padre
 * Un contenido pagina => nodo hijo
 * @author usuario
 */
class NodoNavegacion extends ModeloBase {
    
    //******************
    //Parametros basicos
    //******************
    //<editor-fold> 
    protected $table = 'nodos_navegacion';
    
    protected $fillable = [
        'nombre','cuerpo'
    ];
    
    public static $messages = [];
    
    public static $attributesNames= [
    ];
    
    public static function messages()
    {
        return [];
    }
    
    public static function rules($esEnlace, $id)
    {
        if($esEnlace){
            return [
                "nombre" => "required|string|max:64|unique:nodos_navegacion,nombre," . $id . ",id,deleted_at,NULL",
            ];
        }else
        {
            return [
                "nombre" => "required|string|max:64|unique:nodos_navegacion,nombre," . $id . ",id,deleted_at,NULL",
                "cuerpo" => "required"
                
            ];
        }
    }
    //</editor-fold> 
    
    //********************
    //Funciones auxiliares
    //********************
    //<editor-fold> 
    public function esEnlace(){
        return (isset($this->cuerpo));
    }
    
    public function esPagina(){
        return (!$this->esEnlace());
    }
    //</editor-fold> 
    
    //********************
    //Funciones Estáticas
    //********************
    //<editor-fold> 
    
    /*
     * Genera listado para datatable
     */
    public static function getListado($data){
        $query = NodoNavegacion::select(DB::raw("
                                nombre,
                                (CASE 
                                      WHEN (cuerpo IS NULL) THEN 'Enlace'
                                      ELSE 'Página'
                                 END) as tipo,
                                TO_CHAR(created_at,'DD/MM/YYYY') as creado, 
                                TO_CHAR(updated_at,'DD/MM/YYYY') as actualizado,
                                id")
                            );
        
        //Filtros
        $query->likeUpper('nombre',$data['nombre']);
        
        if($data['tipo'] == 1){ //tipo =  1 es Enlace, 2 es Página
            $query->whereNull("cuerpo");
        }
        if($data['tipo'] == 2){
            $query->whereNotNull("cuerpo");
        }
       
        return $query;
    }
    
    /*
     * Devuelve todos los nodos padres (o contenidos enlace) del sistema
     */
    public static function getNodosPadres(){
        $nodosPadre = NodoNavegacion::whereNotNull("cuerpo")->get();
        return $nodosPadre;
    }
    //<editor-fold> 
    //********************
    //Relaciones
    //********************
    //<editor-fold> 
    public function hijos() {
        return $this->belongsToMany('App\Models\NodoNavegacion','nodos_padre_hijo', 
                'nodo_padre_id', 'nodo_hijo_id');      
    }
    
    public function padres() {
        return $this->belongsToMany('App\Models\NodoNavegacion','nodos_padre_hijo', 
                'nodo_hijo_id', 'nodo_padre_id');      
    }
    //</editor-fold> 
    
    //TODO: Validaciones deberían ir en el modelo
}