@extends('backend.layouts.master')

@section('title', 'Editando Rol')

@section('content')
<h1>Contenidos</h1>
<h3>Editando {{ $nodo->nombre }}</h3>

{!! Form::model($nodo, array('route' => array('backend.nodos.update', $nodo->id), 'class' => 'form-horizontal', 'id'=>'create_form')) !!} 
    @include('backend/nodo/shared/form')

{!! Form::close() !!}

@endsection
