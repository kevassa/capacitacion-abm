<?php

namespace App\Models;

use Zizaco\Entrust\EntrustRole;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

use Carbon\Carbon;

use DB;

class Role extends EntrustRole
{
    use SoftDeletes;

    const SUPERADMIN = 'SUPERADMIN';
    const ADMINISTRADOR = 'ADMINISTRADOR';
    const JURADONACIONAL = 'JURADO NACIONAL';
    const JURADOPROVINCIAL = 'JURADO PROVINCIAL';
    const ESPECIALISTA = 'ESPECIALISTA';
    const EDITORIAL = 'EDITORIAL';

    //use \Venturecraft\Revisionable\RevisionableTrait;

    protected $fillable = ['name', 'display_name', 'description', 'activo'];

    public static function rules($id){ 
        return[
            'name' => 'required|unique:roles,name,' . $id,
        ];
    }

    public static $messages = [
        'name.unique' => 'rol/form.rol_duplicado'
    ];

    public function getPermisosDescAttribute(){
            $permisos = array();
            foreach ($this->perms as $permiso){
                    $permisos[] = $permiso->display_name;
            }
            return implode(", ", $permisos);
    }

    public function hasPermission($permission_name){
            foreach ($this->perms as $permiso){
                    if($permiso->name == $permission_name){
                        return true;
                    }
            }
            return false;
    }
    
    /**
     * Scope a query to match roles who has a permission.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $perm_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasPermission($query, $perm_id)
    {
        if(!is_null($perm_id)){
            return $query->whereHas('perms', function ($q) use($perm_id) {
        	    $q->where('id', '=', $perm_id);
        	});
        }else{
            return $query;
        }
    }

     public function getCreatedAtAttribute($value)
    {
        if(empty($value))
            return null;

        return (new Carbon($value))->format('d/m/Y');
    }

    public function getDescEstadoAttribute($value){
        if($this->activo==1){
            return "ACTIVO";
        }else{
            return "INACTIVO";
        }

    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /*
     * Sobreescribo la relacion ya que incorporamos timestamps y softdeletes
     */
    public function perms()
    {
        return $this->belongsToMany(Config::get('entrust.permission'), Config::get('entrust.permission_role_table'))->whereNull('permission_role.deleted_at')->withTimestamps();
    }

    /*
     * Sobreescribo el metodo del trait para que permita soft deletes y timestamps
     */
    public function savePermissions($inputPermissions)
    {

        $permisosActuales = $this->perms()->lists('id')->toArray();

        $syncArray = array();

        // Soft Delete a los que borro
        foreach ($permisosActuales as $permActual){
            if(!in_Array($permActual, $inputPermissions)){
                $syncArray[$permActual] = ['deleted_at' => DB::raw('NOW()')];
            }
        }

        // Fecha de creacion a los nuevos
        foreach ($inputPermissions as $perm){
            if(!in_Array($perm, $permisosActuales)){
                $syncArray[$perm] = ['created_at' => DB::raw('NOW()'), 'deleted_at' => null];
            }
        }
        // Mantengo los que siguen estando
        foreach ($inputPermissions as $perm){
            if(in_Array($perm, $permisosActuales)){
                $syncArray[$perm] = ['deleted_at' => null];
            }
        }

        if (!empty($syncArray)) {

            foreach($syncArray as $perm){
                $this->perms()->sync($syncArray);
            }

        } else {
            $this->perms()->detach();
        }
    }

    public static function bootSoftDeletingTrait(){
        //Para que entrust haga soft deletes
    }
    
    public function scopeLikeUpper($query, $column, $value)
    {
        if(!is_null($value)){
            return $query->where(DB::raw('upper('.$column.')'),'LIKE', '%'.strtoupper($value).'%');
        }else{
            return $query;
        }
    }
}