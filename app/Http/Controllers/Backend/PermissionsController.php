<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use App\Models\Permission;

class PermissionsController extends Controller
{

    public function index(Request $request)
    {
    	
    	$data = $request->input();
    	
    	$name = isset($data['name']) ? $data['name'] : null;
    	$display_name = isset($data['display_name']) ? $data['display_name'] : null;
    	$description = isset($data['description']) ? $data['description'] : null;
    	$perm_id = isset($data['perm_id']) && trim($data['perm_id']) != "0" ? $data['perm_id'] : null;
        
        $sort= isset($data['s']) ? $data['s'] : 'id';
        $dir= isset($data['o']) ? $data['o'] : 'asc';
    	
    	$permissions = Permission::orderBy($sort, $dir)
                    	->likeUpper('name', $name)
                    	->likeUpper('display_name', $display_name)
                    	->likeUpper('description', $description)
    	;

        \Session::put('permissionsList', $permissions->get());
        
        return view('backend/permission/list')
                ->with('permissions', $permissions->paginate())
                ->with('name', $name)
                ->with('display_name', $display_name)
                ->with('description', $description)
        ;
    }
    
    public function xls(){
    	return \Excel::create('Permisos', function($excel) {
    		 
    		$excel->sheet('Hoja 1', function($sheet) {
    			$sheet->row(1, array('ID', 'NOMBRE', 'DISPLAY NAME', 'DESCRIPCION'));
    			$contador = 2;
    			foreach (\Session::get('permissionsList') as $tp){
    				$sheet->row($contador, array(
    						$tp->id,
    						$tp->name,
    						$tp->display_name,
    						$tp->description
    				));
    				$contador++;
    			}
    		});
    
    	})->download('xls');
    }

}
